include("api", "java-sdk")

buildCache {
    local {
        removeUnusedEntriesAfterDays = 1
    }
}

pluginManagement {
    repositories {
        jcenter()
        gradlePluginPortal()
    }
}
