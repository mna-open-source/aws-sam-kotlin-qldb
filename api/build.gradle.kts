import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.5.21")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.5.21")

    //jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-ion:2.12.5")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-cbor:2.12.5")

    //JWT
    implementation("com.nimbusds:nimbus-jose-jwt:9.13")

    //aws
    implementation(platform("software.amazon.awssdk:bom:2.11.6"))
    implementation("com.amazonaws:aws-lambda-java-core:1.2.1")
    implementation("com.amazonaws:aws-lambda-java-events:3.10.0")
    implementation("software.amazon.qldb:amazon-qldb-driver-java:2.3.1")
    implementation("com.amazonaws:aws-java-sdk-qldb:1.12.62")

    //logging
    implementation("org.slf4j:slf4j-api:1.7.32")
    implementation("ch.qos.logback:logback-core:1.2.5")
    implementation("ch.qos.logback:logback-classic:1.2.5")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.5.21")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("io.jsonwebtoken:jjwt:0.9.1")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.apache.logging.log4j:log4j-slf4j18-impl:2.14.1"){
        exclude("org.slf4j","slf4j-api")
    }
    testImplementation("org.testcontainers:localstack:1.16.0")
    testImplementation("org.hamcrest:hamcrest-library:2.2")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}
