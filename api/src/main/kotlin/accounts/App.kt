package accounts

import java.io.FileNotFoundException
import java.io.InputStream
import java.time.LocalDateTime
import java.util.*

object App {

    const val name = "Accounts basic management"

    var dateTimeProvider: IDateTimeProvider = DateTimeProvider()

    fun version() = Properties().merge(versionPropertyResource()).getVersion() ?: "DEVELOPMENT"

    private fun Properties.merge(stream: InputStream) = this.apply { this.load(stream) }

    private fun Properties.getVersion() = this.getProperty("version")

    private fun versionPropertyResource(): InputStream =
            javaClass.classLoader.getResourceAsStream("version.properties") ?:
            throw FileNotFoundException("property file 'version.properties' not found in the classpath")

    fun now(): LocalDateTime = dateTimeProvider.now()

}


fun now(): LocalDateTime = App.now()
