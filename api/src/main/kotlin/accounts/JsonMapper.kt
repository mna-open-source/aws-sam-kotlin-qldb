package accounts

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.math.BigDecimal
import java.time.temporal.ChronoUnit
import java.util.*

open class JsonMapper() : ObjectMapper() {

    init {
        registerKotlinModule()
        registerJavaTimeModule()
        enableCaseInsensitive()
        disableFailOnUnknownProperties()
        disableWriteDatesAsTimeStamps()
    }
}

object Mapper : JsonMapper()

fun ObjectMapper.enableCaseInsensitive(): ObjectMapper = this.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)

fun ObjectMapper.disableFailOnUnknownProperties() : ObjectMapper = this.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

fun ObjectMapper.disableWriteDatesAsTimeStamps() : ObjectMapper = this.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

fun ObjectMapper.registerJavaTimeModule() : ObjectMapper = this.registerModule(JavaTimeModule())

fun <T> String.deserializeAs(targetClass: Class<T>): T = Mapper.readValue(this, targetClass)

fun Date.removeMillisecond(): Date = Date.from(this.toInstant()
        .truncatedTo(ChronoUnit.SECONDS))

fun Double?.asBigDecimal(): BigDecimal? {
    return if (this != null) BigDecimal(this) else null
}

fun <T> T.toJsonString(): String = Mapper.writeValueAsString(this)
