package accounts

import java.time.Clock
import java.time.LocalDateTime

interface IDateTimeProvider {

    fun now(): LocalDateTime

}

class DateTimeProvider: IDateTimeProvider {

    override fun now(): LocalDateTime = LocalDateTime.now(Clock.systemUTC())

}
