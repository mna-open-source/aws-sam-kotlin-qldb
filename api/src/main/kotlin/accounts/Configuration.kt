package accounts


object Configuration {

    /*
    private val log = LoggerFactory.getLogger(Configuration::class.java)
    private lateinit var cachedDynamoDbUserCards: DynamoDbUserCardRepository
    private lateinit var cachedSecurity: Security
    private lateinit var cachedDynamoDBMercadoPagoCustomersRepository: DynamoDBMercadoPagoCustomersRepository
    private lateinit var cachedDefaultUserCards: DefaultUserCards
    private lateinit var cachedMercadoPagoPaymentGateways: Map<Currency, MercadoPagoPaymentGateway>

    fun security(): Security {
        if (!this::cachedSecurity.isInitialized) {
            val signingKey = getSetting("OAUTH_SIGNINGKEY")
            val domain = getSetting("AUTH0_DOMAIN")
            val audience = getSetting("AUTH0_AUDIENCE")

            log.info("Initializing security with signing key {} ..., $domain, $audience", signingKey.take(5))
            cachedSecurity = DefaultSecurity(TokenDecoder(domain, audience, signingKey))
        }
        return cachedSecurity
    }

    private fun userCardRepository(): UserCardRepository {
        if (!this::cachedDynamoDbUserCards.isInitialized) {
            val table = getSetting("USER_CARD_TABLE_V2")

            log.info("Initializing dynamodb userCard repository on table: {} ", table)
            cachedDynamoDbUserCards = DynamoDbUserCardRepository(
                    dynamoDBClient = DynamoDbClient.create(),
                    table = table,
                    cardFilterConverter = CardFilterToPagedQueryParamsConverter(expiredCardCurrencies())
            )
        }
        return cachedDynamoDbUserCards
    }

    fun expiredCardCurrencies() = System.getProperty("EXPIRED_CARD_CURRENCIES", System.getenv("EXPIRED_CARD_CURRENCIES"))
            .split(",")
            .filter { it.isNotEmpty() }
            .map { Currency.valueOf(it) }

    fun userCards(): UserCards {
        if (!this::cachedDefaultUserCards.isInitialized) {
            cachedDefaultUserCards = DefaultUserCards(userCardRepository(), ingenicoPaymentGateways(), Clock.systemUTC(), mercadoPagoSdkPaymentGateways())
        }
        return cachedDefaultUserCards
    }

    fun ingenicoPaymentGateways() = mapOf(
            ARS to IngenicoSdkConfiguration(
                    endpointHost = URI.create(getSetting("INGENICO_API_ENDPOINT")),
                    apiKey = getSetting("INGENICO_API_KEY"),
                    apiSecret = getSetting("INGENICO_API_SECRET"),
                    merchantId = getSetting("INGENICO_MERCHANT_ARS"),
                    connectTimeout = getSetting("INGENICO_CONNECT_TIMEOUT").toInt(),
                    socketTimeout = getSetting("INGENICO_SOCKET_TIMEOUT").toInt(),
                    maxConnections = getSetting("INGENICO_MAX_CONNECTIONS").toInt(),
                    integrator = getSetting("INGENICO_INTEGRATOR")
            ),
            Currency.BRL to IngenicoSdkConfiguration(
                    endpointHost = URI.create(getSetting("INGENICO_API_ENDPOINT")),
                    apiKey = getSetting("INGENICO_API_KEY"),
                    apiSecret = getSetting("INGENICO_API_SECRET"),
                    merchantId = getSetting("INGENICO_MERCHANT_BRL"),
                    connectTimeout = getSetting("INGENICO_CONNECT_TIMEOUT").toInt(),
                    socketTimeout = getSetting("INGENICO_SOCKET_TIMEOUT").toInt(),
                    maxConnections = getSetting("INGENICO_MAX_CONNECTIONS").toInt(),
                    integrator = getSetting("INGENICO_INTEGRATOR")
            )
    ).let { IngenicoSdkPaymentGateways(it.mapValues {
        HttpIngenicoSdk(it.value)
    }, userCardRepository(), mapOf( ARS to ONE)) }

    fun mercadoPagoSdkPaymentGateways() = run {
        if (!this::cachedMercadoPagoPaymentGateways.isInitialized) {
            val restTemplate = RestTemplate()
            cachedMercadoPagoPaymentGateways = mapOf(
                    ARS to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_ARS")),
                    Currency.BRL to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_BRL")),
                    Currency.CLP to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_CLP")),
                    Currency.MXN to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_MXN")),
                    Currency.PEN to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_PEN")),
                    Currency.UYU to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_UYU")),
                    Currency.COP to HttpMercadoPagoBridge(restTemplate, getSetting("MERCADOPAGO_ACCESS_TOKEN_COP"))

            ).mapValues { MercadoPagoSdkPaymentGateway(it.value) }

        }
        cachedMercadoPagoPaymentGateways
    }

    fun mercadoPagoCustomersRepository(): MercadoPagoCustomersRepository {
        if (!this::cachedDynamoDBMercadoPagoCustomersRepository.isInitialized) {
            val table = getSetting("MERCADOPAGO_CUSTOMERS_TABLE")

            log.info("Initializing dynamodb mercadoPagoCustomers repository on table $table")

            cachedDynamoDBMercadoPagoCustomersRepository = DynamoDBMercadoPagoCustomersRepository(DynamoDbClient.create(), table)
        }
        return cachedDynamoDBMercadoPagoCustomersRepository
    }

    private fun getSetting(key: String): String = System.getProperty(key, System.getenv(key)).apply {
        if (this.isBlank()) throw IllegalArgumentException("setting $key is required")
    }

     */
}
