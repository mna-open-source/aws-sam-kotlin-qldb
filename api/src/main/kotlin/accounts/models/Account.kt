package accounts.models

import accounts.App.now
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.ArrayList

interface IAccount {
    val accountId: String
    val balances: List<Money>
    val locks: List<FundLock>

    fun sumByLocks(): List<Money> = now().let { date ->
        locks.filter { it.dueDate > date }
            .map { it.money }
    }
        .groupingBy { it.key }
        .reduce { _, accumulator, element -> accumulator + element }
        .values.toList()
        .map { -it }

    fun availableBalances(): List<Money> = (balances + sumByLocks())
        .groupingBy { it.key }
        .reduce { _, accumulator, element -> accumulator + element }
        .values.toList()
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Account @JsonCreator constructor(
    @field:JsonProperty("account_id")
    @param:JsonProperty("account_id")
    override val accountId: String,

    @field:JsonProperty("balances")
    @param:JsonProperty("balances")
    override val balances: List<Money> = ArrayList(),

    @field:JsonProperty("locks")
    @param:JsonProperty("locks")
    override val locks: List<FundLock> = ArrayList()
) : IAccount {

    val available: List<Money>
        @JsonProperty("available")
        get() = availableBalances()
}



