package accounts.models

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

@JsonIgnoreProperties(ignoreUnknown = true)
data class Movement @JsonCreator constructor(
    @field:JsonProperty("account_id")
    @param:JsonProperty("account_id")
    val accountId: String,

    @field:JsonProperty("created")
    @param:JsonProperty("created")
    val created: LocalDateTime,

    @field:JsonProperty("money")
    @param:JsonProperty("money")
    val money: Money,

    @field:JsonProperty("transaction_id")
    @param:JsonProperty("transaction_id")
    val transactionId: String
)
