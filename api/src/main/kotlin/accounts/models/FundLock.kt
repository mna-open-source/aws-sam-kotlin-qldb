package accounts.models

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime
import java.util.*

data class FundLock(
    @field:JsonProperty("id")
    @param:JsonProperty("id")
    val id: UUID = UUID.randomUUID(),

    @field:JsonProperty("created")
    @param:JsonProperty("created")
    val created: LocalDateTime,

    @field:JsonProperty("dueDate")
    @param:JsonProperty("dueDate")
    val dueDate: LocalDateTime,

    @field:JsonProperty("money")
    @param:JsonProperty("money")
    val money: Money
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FundLock

        if (created != other.created) return false
        if (dueDate != other.dueDate) return false
        if (money != other.money) return false

        return true
    }

    override fun hashCode(): Int {
        var result = created.hashCode()
        result = 31 * result + dueDate.hashCode()
        result = 31 * result + money.hashCode()
        return result
    }
}
