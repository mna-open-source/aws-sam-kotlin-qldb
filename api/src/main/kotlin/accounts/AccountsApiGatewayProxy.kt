package accounts

import accounts.functions.*
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.ApiGatewayProxy
import rest2Lambda.RestMappings

class AccountsApiGatewayProxy : ApiGatewayProxy() {

    companion object {
        private const val BASE_RESOURCE = "/accounts/{accountId}"
    }

    override fun setup(context: Context) {
        RestMappings
            .get(path = BASE_RESOURCE, handler = GetBalanceFunction())
            .post(path = "${BASE_RESOURCE}/credits", handler = ApplyCreditFunction())
            .post(path = "${BASE_RESOURCE}/debits", handler = ApplyDebitFunction())
            .post(path = "${BASE_RESOURCE}/locks", handler = LockFundsFunction())
            .delete(path = "${BASE_RESOURCE}/locks/{lockId}", handler = ReleaseLockFundsFunction())
            .post(path = "${BASE_RESOURCE}/transfers", handler = TransferFunction())
            .get(path = "/blocks/{strandId}/{sequenceNo}", handler = GetBlockFunction())
        QLDBConnector.createTablesIfNotExists()
    }

    override fun preHandleRequest(input: APIGatewayProxyRequestEvent, context: Context) {
        listOf(
            "invoked: ${App.name} - version ${App.version()}\r\n",
            "input: ${input.toJsonString()}\r\n"
        ).forEach { context.logger?.log(it) }
    }

}
