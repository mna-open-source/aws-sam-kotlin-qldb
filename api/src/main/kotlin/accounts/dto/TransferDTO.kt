package accounts.dto

import accounts.Mapper
import accounts.models.Money
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.type.TypeReference

@JsonIgnoreProperties(ignoreUnknown = true)
data class TransferDTO @JsonCreator constructor(

    @field:JsonProperty("transaction_id")
    @param:JsonProperty("transaction_id")
    val transactionId: String,

    @field:JsonProperty("target")
    @param:JsonProperty("target")
    val target: String,

    @field:JsonProperty("money")
    @param:JsonProperty("money")
    val money: Money
)

fun APIGatewayProxyRequestEvent.getBodyAsTransferDTOList(): List<TransferDTO> =
    Mapper.readValue(this.body, object : TypeReference<List<TransferDTO>>() {})
