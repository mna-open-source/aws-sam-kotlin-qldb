package accounts.dto

import accounts.Mapper
import accounts.models.Money
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class TransactionDTO @JsonCreator constructor(
    @field:JsonProperty("transaction_id")
    @param:JsonProperty("transaction_id")
    val transactionId: String,

    @field:JsonProperty("money")
    @param:JsonProperty("money")
    val money: Money
) {
    operator fun unaryMinus() = copy(money = -money)
}

fun APIGatewayProxyRequestEvent.getBodyAsTransactionDTO(): TransactionDTO =
    Mapper.readValue(this.body, TransactionDTO::class.java)

fun APIGatewayProxyRequestEvent.getBodyAsMoney(): Money = Mapper.readValue(this.body, Money::class.java)

