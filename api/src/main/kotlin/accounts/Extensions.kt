package accounts

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import java.util.*

fun APIGatewayProxyRequestEvent.toString(headerToExclude: String): String {
    val nonSensibleHeaders = this.headers?.filter { it.key != headerToExclude }
    val nonSensibleMultiValueHeaders = this.multiValueHeaders?.filter { it.key != headerToExclude }
    return listOf(
            "resource" to resource,
            "path" to path,
            "httpMethod" to httpMethod,
            "headers" to nonSensibleHeaders,
            "multiValueHeaders" to nonSensibleMultiValueHeaders,
            "queryStringParameters" to queryStringParameters,
            "multiValueQueryStringParameters" to multiValueQueryStringParameters,
            "pathParameters" to pathParameters,
            "stageVariables" to stageVariables,
            "requestContext" to requestContext,
            "isBase64Encoded" to isBase64Encoded
    )
            .filter { it.second != null }
            .joinToString(",") { "${it.first}: ${it.second}" }
}

fun APIGatewayProxyRequestEvent.getAccountId() : String = this.pathParameters["accountId"] ?: throw AccountIdNotProvided()

fun APIGatewayProxyRequestEvent.getLockId() : String = this.pathParameters["lockId"] ?: throw LockIdNotProvided()

fun <F, S, R> withPair(first: F, second: S, block: (F, S) -> R): R  = block(first, second)

fun Context.log(message: String) = this.logger!!.log(message)

fun String.asUUID(): UUID = UUID.fromString(this)
