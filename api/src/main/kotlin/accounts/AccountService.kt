package accounts

import accounts.dto.TransactionDTO
import accounts.models.*
import software.amazon.qldb.TransactionExecutor
import java.util.*

const val LOCK_DUE_SECONDS: Long = 3600

fun fetchAccount(accountId: String): Account =
    QLDBConnector.execute { txn -> fetchAccount(txn, accountId) }

fun fetchAccount(txn: TransactionExecutor, accountId: String): Account =
    QLDBDao.createAccountIfNotExists(txn, accountId)


fun Account.addMovement(transaction: TransactionDTO): Movement =
    addMovement(transaction.transactionId, transaction.money)

fun Account.addMovement(txn: TransactionExecutor, transaction: TransactionDTO): Movement =
    addMovement(txn, transaction.transactionId, transaction.money)

fun Account.addMovement(transactionId: String, money: Money): Movement =
    QLDBConnector.execute { txn -> addMovement(txn, transactionId, money) }

fun Account.addMovement(txn: TransactionExecutor, transactionId: String, money: Money): Movement =
    Movement(accountId, now(), money, transactionId).also {
        QLDBDao.insertMovement(txn, accountId, it)
    }

fun Account.canDebitAmount(money: Money) = also {
    availableBalances().findLast { money.isCompatible(it) }.also { balance ->
        if (balance == null || balance < money) throw InsufficientFunds()
    }
}

fun Account.addLock(money: Money): UUID =
    QLDBConnector.execute { txn -> addLock(txn, money) }

fun Account.addLock(txn: TransactionExecutor, money: Money): UUID =
    FundLock(created = now(), dueDate = now().plusSeconds(LOCK_DUE_SECONDS), money = money).let { lock ->
        QLDBDao.insertLock(txn, accountId, lock)
        lock.id
    }

fun Account.removeLock(id: UUID) = also {
    QLDBConnector.execute { txn -> removeLock(txn, id) }
}

fun Account.removeLock(txn: TransactionExecutor, id: UUID) = also {
    QLDBDao.removeLock(txn, accountId, id)
}

fun Account.getLockById(lockId: UUID): FundLock? = let {
    QLDBConnector.execute { txn -> getLockById(txn, lockId) }
}

fun Account.getLockById(txn: TransactionExecutor, lockId: UUID): FundLock? = let {
    QLDBDao.findAccountLockById(txn, accountId, lockId)
}
