package accounts.functions

import accounts.*
import accounts.dto.getBodyAsTransactionDTO
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.*

class ApplyCreditFunction : RestHandler {

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        context.log("running ${functionName()}...\r\n").let {
            try {
                withPair(input.getAccountId(), input.getBodyAsTransactionDTO()) { accountId, transaction ->
                    QLDBConnector.execute { txn ->
                        fetchAccount(txn, accountId)
                            .addMovement(txn, transaction)
                            .noContent()
                            .addFunctionNameResponseHeader(functionName())
                            .addQLDBTranssctionResponseHeader(txn.transactionId)
                    }
                }
            } catch (e: RestHandlerException) {
                e.functionName = functionName()
                throw e
            }
        }
}
