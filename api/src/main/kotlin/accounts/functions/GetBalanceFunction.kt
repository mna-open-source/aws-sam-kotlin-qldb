package accounts.functions

import accounts.fetchAccount
import accounts.getAccountId
import accounts.log
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.RestHandler
import rest2Lambda.RestHandlerException
import rest2Lambda.addFunctionNameResponseHeader
import rest2Lambda.ok

class GetBalanceFunction : RestHandler {

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        context.log("running ${functionName()}...\r\n").let {
            try {
                fetchAccount(input.getAccountId())
                    .ok()
                    .addFunctionNameResponseHeader(functionName())

            } catch (e: RestHandlerException) {
                e.functionName = functionName()
                throw e
            }
        }
}
