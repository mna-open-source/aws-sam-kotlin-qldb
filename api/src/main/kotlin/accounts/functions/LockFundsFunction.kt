package accounts.functions

import accounts.*
import accounts.dto.getBodyAsMoney
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.*

class LockFundsFunction : RestHandler {

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        context.log("running ${functionName()}...\r\n").let {
            try {
                withPair(input.getAccountId(), input.getBodyAsMoney()) { accountId, money ->
                    QLDBConnector.execute { txn ->
                        fetchAccount(txn, accountId)
                            .canDebitAmount(money)
                            .addLock(txn, money)
                            .ok()
                            .addFunctionNameResponseHeader(functionName())
                            .addQLDBTranssctionResponseHeader(txn.transactionId)
                    }
                }
            } catch (e: RestHandlerException) {
                e.functionName = functionName()
                throw e
            }
        }
}
