package accounts.functions

import accounts.*
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.*

class GetBlockFunction : RestHandler {
    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        context.log("running ${functionName()}...\r\n").let {
            try {
                withPair(input.getStrandId(), input.getSequenceNo()) { strandId, sequenceNo ->

                    QLDBConnector.getBlock(strandId, sequenceNo).let { response ->

                        when (response.sdkHttpMetadata.httpStatusCode) {
                            200 -> {
                                QLDBConnector.MAPPER.readTree(response.block.ionText)
                                    .ok()
                            }
                            404 -> notFound("/blocks/$strandId/$sequenceNo")
                            else -> badGateway("QLDB returns status ${response.sdkHttpMetadata.httpStatusCode}")
                        }
                    }.addFunctionNameResponseHeader(functionName())
                }
            } catch (e: RestHandlerException) {
                e.functionName = functionName()
                throw e
            }
        }
}

fun APIGatewayProxyRequestEvent.getStrandId(): String = this.pathParameters["strandId"]
    ?: throw StrandIdNotProvided()

fun APIGatewayProxyRequestEvent.getSequenceNo(): Long = this.pathParameters["sequenceNo"]?.toLong()
    ?: throw SequenceNoNotProvided()

class StrandIdNotProvided : RestHandlerException(400, "Expected {strandId} path parameter was not found")

class SequenceNoNotProvided : RestHandlerException(400, "Expected {strandId} path parameter was not found")
