package accounts.functions

import accounts.*
import accounts.dto.TransactionDTO
import accounts.dto.getBodyAsTransferDTOList
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.*

class TransferFunction : RestHandler {

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        context.log("running ${functionName()}...\r\n").let {
            try {
                withPair(input.getAccountId(), input.getBodyAsTransferDTOList()) { accountId, transfers ->
                    QLDBConnector.execute { txn ->
                        transfers.forEach { transfer ->
                            withPair(
                                fetchAccount(txn, accountId),
                                fetchAccount(txn, transfer.target)
                            ) { sourceAccount, targetAccount ->
                                sourceAccount
                                    .canDebitAmount(transfer.money)
                                    .addMovement(
                                        txn, TransactionDTO(
                                            transactionId = transfer.transactionId,
                                            money = -transfer.money
                                        )
                                    )
                                targetAccount
                                    .addMovement(
                                        txn, TransactionDTO(
                                            transactionId = transfer.transactionId,
                                            money = transfer.money
                                        )
                                    )
                            }
                        }
                            .noContent()
                            .addFunctionNameResponseHeader(functionName())
                            .addQLDBTranssctionResponseHeader(txn.transactionId)
                    }
                }
            } catch (e: RestHandlerException) {
                e.functionName = functionName()
                throw e
            }
        }
}
