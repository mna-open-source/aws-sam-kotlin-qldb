package accounts.functions

import accounts.*
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import rest2Lambda.*

class ReleaseLockFundsFunction : RestHandler {

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        context.log("running ${functionName()}...\r\n").let {
            try {
                withPair(input.getAccountId(), input.getLockId()) { accountId, lockId ->
                    QLDBConnector.execute { txn ->
                        fetchAccount(txn, accountId)
                            .removeLock(txn, lockId.asUUID())
                            .noContent()
                            .addFunctionNameResponseHeader(functionName())
                            .addQLDBTranssctionResponseHeader(txn.transactionId)
                    }
                }
            } catch (e: RestHandlerException) {
                e.functionName = functionName()
                throw e
            }
        }
}
