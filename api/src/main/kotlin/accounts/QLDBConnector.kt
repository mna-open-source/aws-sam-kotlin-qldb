package accounts

import com.amazon.ion.IonSystem
import com.amazon.ion.IonValue
import com.amazon.ion.system.IonReaderBuilder
import com.amazon.ion.system.IonSystemBuilder
import com.amazon.ion.system.IonTextWriterBuilder
import com.amazonaws.services.qldb.AmazonQLDB
import com.amazonaws.services.qldb.AmazonQLDBClientBuilder
import com.amazonaws.services.qldb.model.GetBlockRequest
import com.amazonaws.services.qldb.model.GetBlockResult
import com.amazonaws.services.qldb.model.ValueHolder
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.ion.IonObjectMapper
import com.fasterxml.jackson.dataformat.ion.ionvalue.IonValueMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.slf4j.LoggerFactory
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.qldbsession.QldbSessionClient
import software.amazon.awssdk.services.qldbsession.QldbSessionClientBuilder
import software.amazon.qldb.QldbDriver
import software.amazon.qldb.Result
import software.amazon.qldb.RetryPolicy
import software.amazon.qldb.TransactionExecutor
import java.io.IOException

object QLDBConnector {

    private val log = LoggerFactory.getLogger(QLDBConnector::class.java)
    private var qldbDriver: QldbDriver? = null
    private var credentialsProvider: AwsCredentialsProvider? = null
    private val AWS_REGION = Region.US_WEST_2
    private val SYSTEM: IonSystem = IonSystemBuilder.standard().build()
    val MAPPER: IonObjectMapper = IonValueMapper(SYSTEM)
        .registerModule(JavaTimeModule()) as IonObjectMapper

    private const val RETRY_LIMIT = 4
    const val DEV_LEDGER_NAME = "dev-my-company"
    const val ACCOUNTS_TABLE = "accounts"
    const val ACCOUNT_ID_FIELD = "account_id"
    const val MOVEMENTS_TABLE = "movements"

    private val LEDGER_NAME: String
        get() {
            val ledger = System.getenv()["LEDGER_NAME"] ?: DEV_LEDGER_NAME
            val resolvedLedger = if (ledger == "EnvironmentStackName-my-company") DEV_LEDGER_NAME else ledger
            println("AWS QLDB Connector: Using $resolvedLedger ledger.")
            return resolvedLedger
        }

    private val TABLES = mutableListOf(ACCOUNTS_TABLE, MOVEMENTS_TABLE)
    private val INDEXES = mapOf(
        ACCOUNTS_TABLE to ACCOUNT_ID_FIELD,
        MOVEMENTS_TABLE to ACCOUNT_ID_FIELD
    )


    private fun getClient(): AmazonQLDB = AmazonQLDBClientBuilder.standard().build()

    fun <T> execute(block: (TransactionExecutor) -> T): T = getDriver().execute(block)

    private fun getDriver(): QldbDriver {
        if (qldbDriver == null) {
            qldbDriver = createQldbDriver()
        }
        return qldbDriver!!
    }

    fun createTablesIfNotExists() {
        getDriver().tableNames.toCollection(ArrayList()).also { existingTables ->
            if (existingTables.isEmpty()) createAllTables()
            else {
                TABLES.removeAll(existingTables)
                if (TABLES.isNotEmpty()) {
                    execute { txn ->
                        TABLES.forEach { tableName -> createTable(txn, tableName) }
                    }
                }
            }
        }
    }

    private fun toIonValues(result: Result): List<IonValue> = result.toCollection(ArrayList())

    private fun createAllTables() {
        execute { txn ->
            TABLES.forEach { tableName -> createTable(txn, tableName) }
        }
    }

    private fun createTable(txn: TransactionExecutor, tableName: String): Int =
        txn.execute("CREATE TABLE $tableName").let { toIonValues(it).size }
            .let {
                it + createIndex(
                    txn, tableName, INDEXES[tableName]
                        ?: error("No index for $tableName")
                )
            }

    private fun createIndex(txn: TransactionExecutor, tableName: String, indexAttribute: String): Int =
        txn.execute("CREATE INDEX ON $tableName ($indexAttribute)").let { toIonValues(it).size }

    private fun createQldbDriver(): QldbDriver = QldbDriver.builder()
        .ledger(LEDGER_NAME)
        .transactionRetryPolicy(RetryPolicy.builder().maxRetries(RETRY_LIMIT).build())
        .sessionClientBuilder(getAmazonQldbSessionClientBuilder())
        .build()

    private fun getAmazonQldbSessionClientBuilder(): QldbSessionClientBuilder =
        QldbSessionClient.builder().region(AWS_REGION)

    fun getBlock(strandId: String, sequenceNo: Long): GetBlockResult {
        log.info("Let's get the block for block address $strandId:$sequenceNo of the ledger named $LEDGER_NAME.")
        return try {
            val blockAddress = BlockAddress(strandId, sequenceNo)
            val request: GetBlockRequest = GetBlockRequest()
                .withName(LEDGER_NAME)
                .withBlockAddress(ValueHolder().withIonText(MAPPER.writeValueAsIonValue(blockAddress).toString()))
            val result: GetBlockResult = getClient().getBlock(request)
            log.info("Success. GetBlock: ${result.toUnredactedString()}.")
            result
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }

}

data class BlockAddress @JsonCreator constructor(
    @param:JsonProperty("strandId")
    @field:JsonProperty("strandId")
    val strandId: String,

    @param:JsonProperty("sequenceNo")
    @field:JsonProperty("sequenceNo")
    val sequenceNo: Long
)

fun GetBlockResult.toUnredactedString(): String {
    val sb = StringBuilder()
    sb.append("{")
    if (block != null) {
        sb.append("Block: ").append(block.toUnredactedString()).append(",")
    }
    if (proof != null) {
        sb.append("Proof: ").append(proof.toUnredactedString())
    }
    sb.append("}")
    return sb.toString()
}

fun ValueHolder.toUnredactedString(): String {
    val sb = java.lang.StringBuilder()
    sb.append("{")
    if (ionText != null) {
        sb.append("IonText: ")
        val prettyWriter = IonTextWriterBuilder.pretty().build(sb)
        try {
            prettyWriter.writeValues(IonReaderBuilder.standard().build(ionText))
        } catch (ioe: IOException) {
            sb.append("**Exception while printing this IonText**")
        }
    }
    sb.append("}")
    return sb.toString()
}



