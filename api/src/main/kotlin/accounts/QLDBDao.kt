package accounts

import accounts.models.*
import com.amazon.ion.IonValue
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonProcessingException
import software.amazon.qldb.Result
import software.amazon.qldb.TransactionExecutor
import java.io.IOException
import java.util.*

object QLDBDao {

    private const val INSERT_QUERY = "INSERT INTO %s ?"

    private const val FIND_BY_ID_QUERY = "SELECT * FROM %s BY doc_id WHERE %s = ?"

    private const val DELETE_BY_ID_QUERY = "DELETE FROM %s WHERE %s = ?"

    private const val INSERT_BALANCE_QUERY = """
        FROM accounts AS a
        WHERE a.account_id = ?
        INSERT into a.balances value ?
    """

    private const val COUNT_BALANCES_FOR_ACCOUNT = """
        SELECT count(b) total
        FROM accounts a, @a.balances b
        WHERE a.account_id = ?
        AND b.type = ?
        AND b.currency = ?
    """

    private const val UPDATE_BALANCE_QUERY = """
        FROM accounts a, @a.balances AS b
        WHERE a.account_id = ?
        and b.type = ?
        and b.currency = ?
        set b.amount = b.amount + ?
    """

    private const val INSERT_LOCK_QUERY = """
        FROM accounts AS a
        WHERE a.account_id = ?
        INSERT into a.locks value ?
    """

    private const val GET_LOCK_BY_ID_QUERY = """
        SELECT b.*
        FROM accounts a, @a.locks b
        WHERE a.account_id = ?
        AND b.id = ?
    """

    private const val DELETE_LOCK_BY_ID_QUERY = """
        FROM accounts AS a, @a.locks b
        WHERE a.account_id =  ?
        and b.id = ?
        REMOVE b
    """

    private const val TYPE_DELIMITER = "."
    private const val EMPTY = ""

    fun <T> findBySingleColumns(txn: TransactionExecutor, table: String,
                                columnName: String, columnValue: String,
                                itemClass: Class<T>): List<T> {
        return try {
            val results: MutableList<T> = ArrayList()
            val query = String.format(FIND_BY_ID_QUERY, table, columnName)
            val result: Result = txn.execute(query, QLDBConnector.MAPPER.writeValueAsIonValue(columnValue))
            result.iterator().forEachRemaining { row: IonValue ->
                try {
                    results.add(QLDBConnector.MAPPER.readValue(row, itemClass))
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            results
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }

    fun deleteBySingleColumns(txn: TransactionExecutor, table: String,
                              columnName: String, columnValue: String): List<String> {
        return try {
            val results: MutableList<String> = ArrayList()
            val query = String.format(DELETE_BY_ID_QUERY, table, columnName)
            val result: Result = txn.execute(query, QLDBConnector.MAPPER.writeValueAsIonValue(columnValue))
            result.iterator().forEachRemaining { row: IonValue ->
                val jsonText = row.toPrettyString()
                try {
                    results.add(QLDBConnector.MAPPER.readTree(jsonText).get("documentId").asText())
                } catch (e: JsonProcessingException) {
                    e.printStackTrace()
                }
            }
            results
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }

    fun findAccountById(txn: TransactionExecutor, accountId: String) : Account? =
            findBySingleColumns(txn, QLDBConnector.ACCOUNTS_TABLE, QLDBConnector.ACCOUNT_ID_FIELD,
                    accountId, Account::class.java).firstOrNull()

    fun createAccountIfNotExists(txn: TransactionExecutor, accountId: String) : Account =
            findAccountById(txn, accountId) ?:
            Account(accountId).also {
                insertDocuments(txn, QLDBConnector.ACCOUNTS_TABLE, listOf<Any>(it))
            }


    @Throws(IOException::class)
    private fun accountHasBalance(txn: TransactionExecutor, accountId: String,
                                  moneyType: String, currency: String): Boolean {
        val balancesCount = countBalancesByAccountAndMoneyTypeAndCurrency(txn, accountId, moneyType, currency)
        return balancesCount > 0
    }

    @Throws(IOException::class)
    private fun insertBalance(txn: TransactionExecutor, accountId: String,
                              movement: Movement, type: String) {
        val initialBalance = movement.money.copy(type = type)
        val accountIdValue: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(accountId)
        val balanceDocument: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(initialBalance)
        val resultIds = getDocumentIdsFromDmlResult(txn.execute(INSERT_BALANCE_QUERY, accountIdValue, balanceDocument))
        if (resultIds.isEmpty()) throw RuntimeException("Unable to insert balance")
    }

    @Throws(IOException::class)
    private fun updateBalance(txn: TransactionExecutor, accountId: String,
                              movement: Movement, type: String) {
        val accountIdValue: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(accountId)
        val resultIds = getDocumentIdsFromDmlResult(txn.execute(UPDATE_BALANCE_QUERY, accountIdValue,
                QLDBConnector.MAPPER.writeValueAsIonValue(type),
                QLDBConnector.MAPPER.writeValueAsIonValue(movement.money.currency),
                QLDBConnector.MAPPER.writeValueAsIonValue(movement.money.amount)))
        if (resultIds.isEmpty()) throw RuntimeException("Unable to update balance")
    }

    fun insertLock(txn: TransactionExecutor, accountId: String, fundLock: FundLock) {
        val accountIdValue: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(accountId)
        val lockDocument: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(fundLock)
        val resultIds = getDocumentIdsFromDmlResult(txn.execute(INSERT_LOCK_QUERY, accountIdValue, lockDocument))
        if (resultIds.isEmpty()) throw RuntimeException("Unable to insert lock")
    }

    fun removeLock(txn: TransactionExecutor, accountId: String, id: UUID) {
        val accountIdValue: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(accountId)
        val lockDocumentId: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(id)
        val resultIds = getDocumentIdsFromDmlResult(txn.execute(DELETE_LOCK_BY_ID_QUERY,
                accountIdValue, lockDocumentId))
        if (resultIds.isEmpty()) throw FundLockNotFound(id)
    }

    fun findAccountLockById(txn: TransactionExecutor, accountId: String, lockId: UUID) : FundLock? {
        val results: MutableList<FundLock> = ArrayList()
        val accountIdValue: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(accountId)
        val lockIdValue: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(lockId)

        val result: Result = txn.execute(GET_LOCK_BY_ID_QUERY, accountIdValue, lockIdValue)
        result.iterator().forEachRemaining { row: IonValue ->
            try {
                results.add(QLDBConnector.MAPPER.readValue(row, FundLock::class.java))
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return results.firstOrNull()
    }


    fun insertMovement(txn: TransactionExecutor,
                       accountId: String,
                       movement: Movement) {
        try {
            insertDocuments(txn, QLDBConnector.MOVEMENTS_TABLE, listOf<Any>(movement))
            upsertBalance(txn, accountId, movement)
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }

    private fun upsertBalance(txn: TransactionExecutor, accountId: String,
                              movement: Movement, type: String = movement.money.type) {
        if (accountHasBalance(txn, accountId, type, movement.money.currency)) {
            updateBalance(txn, accountId, movement, type)
        } else {
            insertBalance(txn, accountId, movement, type)
        }
        /*
        * Balance grouping disabled
        *
        type.takeIfHasParentType()?.also {
            upsertBalance(txn, accountId, movement, it)
        }
        */
    }

    /*
    * Balance grouping disabled
    *
    private fun String.takeIfHasParentType(): String? = substringBeforeLast(TYPE_DELIMITER, EMPTY)
            .takeIf { it.isNotEmpty() }

     */


    fun countBalancesByAccountAndMoneyTypeAndCurrency(txn: TransactionExecutor,
                                                      accountId: String,
                                                      moneyType: String,
                                                      currency: String): Int {
        return try {
            val result = txn.execute(COUNT_BALANCES_FOR_ACCOUNT,
                    QLDBConnector.MAPPER.writeValueAsIonValue(accountId),
                    QLDBConnector.MAPPER.writeValueAsIonValue(moneyType),
                    QLDBConnector.MAPPER.writeValueAsIonValue(currency))
            if (result.isEmpty) return 0
            val value = result.iterator().next()
            QLDBConnector.MAPPER.readTree(value.toPrettyString()).get("total").asInt()
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }

    fun insertDocuments(txn: TransactionExecutor, tableName: String?,
                        documents: List<*>?) {
        try {
            val query = String.format(INSERT_QUERY, tableName)
            val ionDocuments: IonValue = QLDBConnector.MAPPER.writeValueAsIonValue(documents)
            if (getDocumentIdsFromDmlResult(txn.execute(query, ionDocuments)).isEmpty()) throw RuntimeException("Unable to insert document")
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }

    fun getDocumentIdsFromDmlResult(result: Result): List<String> {
        val strings: MutableList<String> = ArrayList()
        result.iterator().forEachRemaining { row: IonValue? -> strings.add(getDocumentIdFromDmlResultDocument(row)) }
        return strings
    }

    fun getDocumentIdFromDmlResultDocument(dmlResultDocument: IonValue?): String {
        return try {
            val result: DmlResultDocument = QLDBConnector.MAPPER.readValue(dmlResultDocument, DmlResultDocument::class.java)
            result.documentId
        } catch (ioe: IOException) {
            throw IllegalStateException(ioe)
        }
    }
}

data class DmlResultDocument @JsonCreator constructor(
        @JsonProperty("documentId") val documentId: String)
