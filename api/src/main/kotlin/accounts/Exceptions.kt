package accounts

import rest2Lambda.RestHandlerException
import java.util.*

class AccountIdNotProvided: RestHandlerException(400, "Expected {account_id} path parameter was not found")

class LockIdNotProvided: RestHandlerException(400, "Expected {lock_id} path parameter was not found")

class InsufficientFunds: RestHandlerException(409, "Insufficient Funds.")

class FundLockNotFound(id: UUID): RestHandlerException(404, "Fund lock with ID='${id}' not found")
