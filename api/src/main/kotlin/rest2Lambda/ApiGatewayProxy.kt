package rest2Lambda

import accounts.log
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.nimbusds.jose.JWSVerifier
import com.nimbusds.jose.crypto.MACVerifier
import com.nimbusds.jwt.SignedJWT
import java.text.ParseException

abstract class ApiGatewayProxy: RestHandler {

    companion object {
        private var initialized = false
        private const val AUTHORIZATION_HEADER = "Authorization"
        private const val BEARER_PREFIX = "Bearer"
        val verifier: JWSVerifier = MACVerifier("AsDfjKl12345678AsDfjKl12345678AsDfjKl12345678AsDfjKl12345678")
    }

    abstract fun setup(context: Context)

    open fun preHandleRequest(input: APIGatewayProxyRequestEvent, context: Context) {}

    open fun preAuthorize(jwt: SignedJWT, context: Context) {}

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context): APIGatewayProxyResponseEvent {
        if (!initialized) {
            context.logger.log("configuring rest2Lambda...\r\n")
            setup(context)
            initialized = true
        }
        authorize(input, context)
        preHandleRequest(input, context)
        return try {
             RestMappings.invokeHandler(input, context)
        } catch (re: RestHandlerException) {
            context.log("ERROR: ${re.message}")
            re.toResponse()
        } catch (u: UnAuthorized) {
            context.log("WARNING: Access Denied: ${u.message}")
            accessDenied()
        } catch (t: Throwable) {
            context.log("Unhandled ERROR: ${t.message}")
            t.printStackTrace()
            internalServerError(t.message)
        }
    }

    open fun authorize(input: APIGatewayProxyRequestEvent, context: Context) {
        if (input.headers == null || !input.headers.containsKey(AUTHORIZATION_HEADER)) throw UnAuthorized("Empty Token")

        input.headers[AUTHORIZATION_HEADER]!!.removePrefix(BEARER_PREFIX).trim()
                .let { token ->
                    try {
                        SignedJWT.parse(token).also {
                            if (!it.verify(verifier)) throw UnAuthorized("Invalid signature")
                        }
                    } catch (pe: ParseException) {
                        throw UnAuthorized("Invalid JWT")
                    }
                }
                .also { jwt -> preAuthorize(jwt, context)}
    }

    class UnAuthorized(message: String) : RuntimeException(message)
}
