package rest2Lambda

import java.lang.RuntimeException

abstract class RestHandlerException (val status: Int, message:String) : RuntimeException("$status: $message") {
    var functionName : String = "unknown function"
}
