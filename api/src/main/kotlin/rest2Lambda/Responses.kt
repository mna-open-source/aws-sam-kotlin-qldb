package rest2Lambda

import accounts.Mapper
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent

fun APIGatewayProxyResponseEvent.addFunctionNameResponseHeader(functionName: String) = this
        .also{
            val headers = HashMap<String, String>()
            if (it.headers != null) headers.putAll(it.headers)
            headers["X-Fn"] = functionName
            it.withHeaders(headers)
        }

fun APIGatewayProxyResponseEvent.addQLDBTranssctionResponseHeader(QLDBtransactionId: String) = this
        .also{
            val headers = HashMap<String, String>()
            if (it.headers != null) headers.putAll(it.headers)
            headers["X-QLDB-TX"] = QLDBtransactionId
            it.withHeaders(headers)
        }

fun <T> T.ok() : APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(200)
                .withHeaders(mapOf("Content-Type" to "application/json"))
                .withBody(this.serializeAsJson())

fun <T> T.serializeAsJson() = Mapper.writeValueAsString(this)



fun <T> T.noContent(): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(204)


fun badRequest(body: String): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(400)
                .withBody(body)

fun conflict(body: String): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(409)
                .withBody(body)

fun accessDenied(): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(401)

fun forbidden(body: String): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(403)
                .withBody(body)

fun notFound(path: String): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(404)
                .withBody(path)

fun internalServerError(message: String?): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(500)
                .withBody("{\"message\": \"FATALITY!: ${message ?: "\"Internal Server Error"}\" }")

fun badGateway(message: String?): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(502)
                .withBody("{\"message\": \"${message ?: "\"Bad Gateway"}\" }")

class PathMismatch(path: String) : RuntimeException("invalid path: $path")

fun RestHandlerException.toResponse(): APIGatewayProxyResponseEvent =
        APIGatewayProxyResponseEvent()
                .withStatusCode(status)
                .withBody("{\"message\": \"$message\" }")
                .addFunctionNameResponseHeader(functionName)
