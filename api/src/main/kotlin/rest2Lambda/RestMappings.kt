package rest2Lambda

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent

object RestMappings {
    private val mappings = HashMap<String, RestHandler>()

    fun get(path: String, handler: RestHandler) = addMapping(HttpMethod.GET, path, handler)
    fun post(path: String, handler: RestHandler) = addMapping(HttpMethod.POST, path, handler)
    fun put(path: String, handler: RestHandler) = addMapping(HttpMethod.PUT, path, handler)
    fun patch(path: String, handler: RestHandler) = addMapping(HttpMethod.PATCH, path, handler)
    fun delete(path: String, handler: RestHandler) = addMapping(HttpMethod.DELETE, path, handler)
    fun head(path: String, handler: RestHandler) = addMapping(HttpMethod.HEAD, path, handler)
    fun options(path: String, handler: RestHandler) = addMapping(HttpMethod.OPTIONS, path, handler)

    private fun addMapping(method: HttpMethod, path: String, handler: RestHandler) = RestMappings
            .also { mappings[tokenizeRequestMapping(method, path)] = handler }

    private fun tokenizeRequestMapping(method: HttpMethod, path: String) = "${method}:$path"

    private fun tokenizeRequestMapping(requestEvent: APIGatewayProxyRequestEvent) = requestEvent.let{
        tokenizeRequestMapping(HttpMethod.valueOf(it.httpMethod), it.resource)
    }

    fun invokeHandler(input: APIGatewayProxyRequestEvent, context: Context): APIGatewayProxyResponseEvent =
            tokenizeRequestMapping(input).let { token ->
                mappings[token]?.handleRequest(input, context) ?: notFound(token)
            }
}

