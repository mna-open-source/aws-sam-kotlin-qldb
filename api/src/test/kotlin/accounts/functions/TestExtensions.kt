package accounts.functions

import accounts.AccountMock
import accounts.ContextMock
import accounts.Mapper
import accounts.dto.TransactionDTO
import accounts.dto.TransferDTO
import accounts.models.FundLock
import accounts.models.Money
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.math.roundToInt
import kotlin.random.Random.Default.nextDouble

object TestExtensions {
    val context = ContextMock()
    val logger: Logger = LoggerFactory.getLogger(TestExtensions::class.java)
}

fun anyUUID(): UUID = UUID.randomUUID()

fun anyString(): String = anyUUID().toString().replace("-","")

fun anyAmount() : Double = (nextDouble(0.01, 1000.00) * 100).roundToInt() / 100.0

fun anyAccountId(): String = "tmp_${anyUUID().toString()}"

fun anyAccount(): AccountMock = AccountMock(accountId = anyAccountId())

fun anyValidJWT(): String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ3YWJpcGF5LWJhY2tlbmQifQ.xC46aJsQhLN1pfJaDwcV-EBpKkrPxUnkobN-cAzMQyk"

fun anyMoney(type: String = anyString(), currency: String = anyString(), amount: Double = anyAmount()): Money =
        Money(type = type, currency = currency, amount = amount)

fun anyTransactionDTO(type: String = anyString(), currency: String = anyString(),
                      amount: Double = anyAmount()) : TransactionDTO =
        TransactionDTO(transactionId = anyString(), money = anyMoney(type, currency, amount))

fun anyTransactionDTO(transactionId: String  = anyString(), money : Money = anyMoney()) : TransactionDTO =
        TransactionDTO(transactionId = transactionId, money = money)

fun anyTransferDTO(transactionId: String  = anyString(), target: String,money : Money = anyMoney()): TransferDTO =
        TransferDTO(transactionId = transactionId, target = target, money = money)

fun anyApplyCreditApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/credit"))

fun anyApplyDebitApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/debit"))

fun anyGetAccountApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/default"))

fun anyAddLockApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/lock"))

fun anyReleaseLockApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/releaseLock"))

fun anyTransferApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/transfer"))

fun anyGetBlockApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("Accounts/getBlock"))

private fun loadEventResource(eventName: String) = TestExtensions::class.java.getResource("/events/${eventName}.json").readText()

private fun deserializeEvent(eventAsJson: String) = Mapper.readValue(eventAsJson, APIGatewayProxyRequestEvent::class.java)

fun APIGatewayProxyRequestEvent.setAccountId(accountId: String) = also { event ->
    event.path = event.path.replace("987987987", accountId)
    event.pathParameters["accountId"] = accountId
}

fun APIGatewayProxyRequestEvent.setLockId(lockId: UUID) = also { event ->
    lockId.toString().also {
        event.path = event.path.replace("b893ab93-a418-4412-8cad-e677a7432eea", it)
        event.pathParameters["lockId"] = it
    }
}

fun APIGatewayProxyRequestEvent.setToken(token: String) = also { event ->
        mapOf("Authorization" to token).also { headers ->
            event.headers = headers
        }

}

fun APIGatewayProxyRequestEvent.setBody(element: Any) = also { event ->
    event.body = Mapper.writeValueAsString(element)
}

fun Money.half() = copy(amount = amount.half())

operator fun Money.times(value: Double) = copy(amount = this.amount * value)

fun Double.half() = this / 2.00

operator fun List<FundLock>.contains(lockId: UUID): Boolean = findLast { it.id == lockId } != null

operator fun String?.contains(other: String?): Boolean = this?.contains(other as CharSequence, true) ?: false
