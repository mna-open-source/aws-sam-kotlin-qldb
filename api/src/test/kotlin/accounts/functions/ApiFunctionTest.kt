package accounts.functions

import accounts.*
import accounts.dto.TransactionDTO
import accounts.functions.*
import accounts.models.Money
import com.amazonaws.services.lambda.runtime.events.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import java.time.Clock
import java.time.LocalDateTime
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

//@RunWith(MockitoJUnitRunner.StrictStubs::class)

@Ignore
class ApiFunctionTest {

    companion object {
        val currentDateTime = LocalDateTime.now(Clock.systemUTC())

        init {
            App.dateTimeProvider = object : IDateTimeProvider {
                override fun now(): LocalDateTime = currentDateTime
            }
        }
    }

    @Before
    fun before() {

        QLDBConnector.execute { txn ->
            //txn.execute("delete from accounts a where a.account_id like 'tmp_%'")
            //txn.execute("delete from movements m where m.account_id like 'tmp_%'")
        }
    }

    @Test
    fun `Given a not existent Account, When try to fetch this account, Then return 200 and a newly created account`() {
        anyAccount().also { expectedAccount ->
            anyGetAccountApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 200,
                        expectedRunningClass = GetBalanceFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given a not existent Account, When apply credit (FIAT, ARS), Then return 204`() {
        withPair(anyAccount(), anyTransactionDTO(type = "FIAT", currency = "ARS")) { expectedAccount, transaction ->
            expectedAccount
                .mockCredit(transaction)

            anyApplyCreditApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(transaction)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyCreditFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    /*
    * Balance grouping disabled
    *
    @Test
    fun `Given a not existent Account, When apply credit with multi-level type, Then return 204`() {

        withPair(anyAccount(), anyTransactionDTO(type = "UNO.DOS.TRES", currency = "ARS")) { expectedAccount, transaction ->

            expectedAccount
                    .mockCredit(transaction)

            anyApplyCreditApiGatewayEvent()
                    .setAccountId(expectedAccount.accountId)
                    .setBody(transaction)
                    .also { event ->

                        invokeLambdaAndAssert(event = event, expectedStatus = 204,
                                expectedRunningClass = ApplyCreditFunction::class.java)

                        assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                    }
        }
    }

    @Test
    fun `Given a not existent Account, When apply multi credits with multi-level type, Then return 204`() {

        val UnoDosTresTransaction =  anyTransactionDTO(type = "UNO.DOS.TRES", currency = "ARS")
        val UnoDosTransaction =  anyTransactionDTO(type = "UNO.DOS", currency = "ARS")
        val UnoTransaction =  anyTransactionDTO(type = "UNO", currency = "ARS")
        val expectedAccount = anyAccount()

        expectedAccount
                .mockCredit(UnoDosTresTransaction)
                .mockCredit(UnoDosTransaction)
                .mockCredit(UnoTransaction)

        anyApplyCreditApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setBody(UnoDosTresTransaction)
                .also { event ->
                    invokeLambdaAndAssert(event = event, expectedStatus = 204,
                            expectedRunningClass = ApplyCreditFunction::class.java)
                }
        anyApplyCreditApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setBody(UnoDosTransaction)
                .also { event ->
                    invokeLambdaAndAssert(event = event, expectedStatus = 204,
                            expectedRunningClass = ApplyCreditFunction::class.java)
                }
        anyApplyCreditApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setBody(UnoTransaction)
                .also { event ->
                    invokeLambdaAndAssert(event = event, expectedStatus = 204,
                            expectedRunningClass = ApplyCreditFunction::class.java)
                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }

    }

     */

    @Test
    fun `Given a not existent Account, When apply credit (FIAT, USD), Then return 204`() {
        withPair(anyAccount(), anyTransactionDTO(type = "FIAT", currency = "USD")) { expectedAccount, transaction ->
            expectedAccount
                .mockCredit(transaction)

            anyApplyCreditApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(transaction)
                .also { event ->

                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyCreditFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given a not existent Account, When apply credit (TO_BE_BILLED, ARS), Then return 204`() {
        withPair(
            anyAccount(),
            anyTransactionDTO(type = "TO_BE_BILLED", currency = "ARS")
        ) { expectedAccount, transaction ->

            expectedAccount
                .mockCredit(transaction)

            anyApplyCreditApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(transaction)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyCreditFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds, When apply debit with amount less than to balance, Then return 204`() {
        anyAccount().also { expectedAccount ->

            val amountToTest = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amountToTest)
            val debitTransaction = anyTransactionDTO(money = creditTransaction.money.half())

            expectedAccount
                .mockCredit(creditTransaction)
                .mockDebit(debitTransaction)

            anyApplyDebitApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(debitTransaction)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyDebitFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds, When apply debit with amount equals to balance, Then return 204 and account balance must be zero`() {
        anyAccount().also { expectedAccount ->
            val amountToTest = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amountToTest)
            val debitTransaction = anyTransactionDTO(money = creditTransaction.money)

            expectedAccount
                .mockCredit(creditTransaction)
                .mockDebit(debitTransaction)

            anyApplyDebitApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(debitTransaction)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyDebitFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds, When apply debit with amount greater than to balance, Then return 409 and Insufficient Funds message`() {
        anyAccount().also { expectedAccount ->
            val amountToTest = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amountToTest)
            val debitTransaction = anyTransactionDTO(money = creditTransaction.money * 2.0)

            expectedAccount
                .mockCredit(creditTransaction)


            anyApplyDebitApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(debitTransaction)
                .also { event ->
                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 409,
                        expectedRunningClass = ApplyDebitFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))

                    assertTrue(InsufficientFunds().message in response.body)
                }
        }
    }

    @Test
    fun `Given Account without funds nor locks, When apply fund lock, Then return 409 and Insufficient Funds message`() {
        anyAccount().also { expectedAccount ->
            val amountToTest = anyMoney()

            anyAddLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(amountToTest)
                .also { event ->
                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 409,
                        expectedRunningClass = LockFundsFunction::class.java
                    )
                    val actualAccount = fetchAccount(expectedAccount.accountId)

                    assertEquals(expectedAccount, actualAccount)

                    assertTrue(InsufficientFunds().message in response.body)
                }
        }
    }

    @Test
    fun `Given Account with funds but no locks, When apply fund lock equals to balance, Then return 200`() {
        anyAccount().also { expectedAccount ->
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId)
            val amountToTest = creditTransaction.money

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amountToTest)

            anyAddLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(amountToTest)
                .also { event ->
                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 200,
                        expectedRunningClass = LockFundsFunction::class.java
                    )

                    val actualAccount = fetchAccount(expectedAccount.accountId)
                    assertEquals(expectedAccount, actualAccount)

                    val lockId = UUID.fromString(response.body.replace("\"", ""))
                    assertTrue { lockId in actualAccount.locks }
                }
        }
    }

    @Test
    fun `Given Account with funds but no locks, When apply fund lock greater than balance, Then return 409`() {
        anyAccount().also { expectedAccount ->
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId)
            val amountToTest = creditTransaction.money * 2.0

            expectedAccount
                .mockCredit(creditTransaction)

            anyAddLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(amountToTest)
                .also { event ->
                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 409,
                        expectedRunningClass = LockFundsFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))

                    assertTrue(InsufficientFunds().message in response.body)
                }
        }
    }

    @Test
    fun `Given Account with funds and locks, When apply fund lock less than available balance, Then return 200`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)

            val amountToTest = amount * 0.5

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amountToTest)

            anyAddLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(amountToTest)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 200,
                        expectedRunningClass = LockFundsFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds and locks, When apply fund lock equals to available balance, Then return 200`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val amountToTest = amount * 0.9
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amountToTest)

            anyAddLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(amountToTest)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 200,
                        expectedRunningClass = LockFundsFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds and locks, When apply fund lock greater than available balance, Then return 409`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)

            val amountToTest = amount * 1.5

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amount)

            anyAddLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(amountToTest)
                .also { event ->
                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 409,
                        expectedRunningClass = LockFundsFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))

                    assertTrue(InsufficientFunds().message in response.body)
                }
        }
    }

    @Test
    fun `Given Account with funds ands locks, When apply debit with amount less than to available balance, Then return 204`() {
        anyAccount().also { expectedAccount ->

            val amount = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            val debitTransaction = anyTransactionDTO(money = amount.half())

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockDebit(debitTransaction)

            anyApplyDebitApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(debitTransaction)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyDebitFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds ands locks, When apply debit with amount greater than to available balance, Then return 409`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)

            val debitTransaction = anyTransactionDTO(money = amount * 1.5)

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amount)

            anyApplyDebitApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(debitTransaction)
                .also { event ->

                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 409,
                        expectedRunningClass = ApplyDebitFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))

                    assertTrue(InsufficientFunds().message in response.body)
                }
        }
    }

    @Test
    fun `Given Account with funds ands locks, When apply debit with amount equals to to available balance, Then return 204`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val amountToTest = amount * 0.9
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)
            lockfunds(accountId = expectedAccount.accountId, money = amount)

            val debitTransaction = anyTransactionDTO(money = amountToTest)

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockFundLock(amount)
                .mockDebit(debitTransaction)

            anyApplyDebitApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(debitTransaction)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ApplyDebitFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds ands locks, When release lock, Then return 204`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount * 0.25)
            lockfunds(accountId = expectedAccount.accountId, money = amount * 0.50)

            val lockToTest = lockfunds(accountId = expectedAccount.accountId, money = amount)

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount * 0.25)
                .mockFundLock(amount * 0.50)

            anyReleaseLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setLockId(lockToTest)
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = ReleaseLockFundsFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                }
        }
    }

    @Test
    fun `Given Account with funds ands locks, When release wrong lock, Then return 409`() {
        anyAccount().also { expectedAccount ->
            val amount = anyMoney()
            val creditTransaction = fundAccount(accountId = expectedAccount.accountId, money = amount * 4.0)

            lockfunds(accountId = expectedAccount.accountId, money = amount * 0.25)
            lockfunds(accountId = expectedAccount.accountId, money = amount * 0.50)

            val idToTest = anyUUID()

            expectedAccount
                .mockCredit(creditTransaction)
                .mockFundLock(amount * 0.25)
                .mockFundLock(amount * 0.50)

            anyReleaseLockApiGatewayEvent()
                .setAccountId(expectedAccount.accountId)
                .setToken(anyValidJWT())
                .setLockId(idToTest)
                .also { event ->
                    val response = invokeLambdaAndAssert(
                        event = event, expectedStatus = 404,
                        expectedRunningClass = ReleaseLockFundsFunction::class.java
                    )

                    assertEquals(expectedAccount, fetchAccount(expectedAccount.accountId))
                    assertTrue(FundLockNotFound(idToTest).message in response.body)
                }

        }
    }

    @Test
    fun `Given source Account with funds and no locks and empty target account, When transfer, Then return 204`() {
        withPair(anyAccount(), anyAccount()) { expectedSourceAccount, expectedTargetAccount ->
            val fiatAmount = anyMoney()
            val toBeBilledAmount = anyMoney()

            val fiatCreditTransaction =
                fundAccount(accountId = expectedSourceAccount.accountId, money = fiatAmount * 4.0)

            val toBeBilledCreditTransaction =
                fundAccount(accountId = expectedSourceAccount.accountId, money = toBeBilledAmount * 4.0)

            val fiatTransferToTest = anyTransferDTO(target = expectedTargetAccount.accountId, money = fiatAmount)

            val toBeBilledTransferToTest =
                anyTransferDTO(target = expectedTargetAccount.accountId, money = toBeBilledAmount)

            expectedSourceAccount
                .mockCredit(fiatCreditTransaction)
                .mockCredit(toBeBilledCreditTransaction)
                .mockTransfer(fiatTransferToTest, expectedTargetAccount)
                .mockTransfer(toBeBilledTransferToTest, expectedTargetAccount)

            anyTransferApiGatewayEvent()
                .setAccountId(expectedSourceAccount.accountId)
                .setToken(anyValidJWT())
                .setBody(arrayOf(fiatTransferToTest, toBeBilledTransferToTest))
                .also { event ->
                    invokeLambdaAndAssert(
                        event = event, expectedStatus = 204,
                        expectedRunningClass = TransferFunction::class.java
                    )

                    assertEquals(expectedSourceAccount, fetchAccount(expectedSourceAccount.accountId))
                    assertEquals(expectedTargetAccount, fetchAccount(expectedTargetAccount.accountId))
                }
        }
    }

    @Test
    fun `Get Block information`() {
        anyGetBlockApiGatewayEvent()
            .setToken(anyValidJWT())
            .also { event ->
                invokeLambdaAndAssert(
                    event = event, expectedStatus = 200,
                    expectedRunningClass = GetBlockFunction::class.java
                )
            }
    }

    private fun invokeLambdaAndAssert(
        event: APIGatewayProxyRequestEvent, expectedStatus: Int,
        expectedRunningClass: Class<*>? = null
    ): APIGatewayProxyResponseEvent =
        AccountsApiGatewayProxy().handleRequest(event, TestExtensions.context).also {
            assertEquals(expectedStatus, it.statusCode)
            assertNotNull(it.headers["X-Fn"])
            expectedRunningClass?.also { clazz ->
                assertEquals(clazz.simpleName, it.headers["X-Fn"])
            }
        }

    private fun fundAccount(accountId: String, money: Money = anyMoney()): TransactionDTO =
        anyTransactionDTO(money = money)
            .also {
                with(
                    anyApplyCreditApiGatewayEvent()
                        .setAccountId(accountId)
                        .setToken(anyValidJWT())
                        .setBody(it)
                ) {
                    AccountsApiGatewayProxy().handleRequest(this, TestExtensions.context).also { response ->
                        assertEquals(204, response.statusCode)
                    }
                }
            }

    private fun lockfunds(accountId: String, money: Money = anyMoney()): UUID = money
        .let {
            anyAddLockApiGatewayEvent()
                .setAccountId(accountId)
                .setToken(anyValidJWT())
                .setBody(money)
        }.let { event ->
            AccountsApiGatewayProxy().handleRequest(event, TestExtensions.context).also { response ->
                assertEquals(200, response.statusCode)
            }
        }.let { response ->
            UUID.fromString(response.body.replace("\"", ""))
        }

}


