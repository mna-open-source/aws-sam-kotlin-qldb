package accounts

import com.amazonaws.services.lambda.runtime.ClientContext
import com.amazonaws.services.lambda.runtime.CognitoIdentity
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger

class ContextMock : Context {

    override fun getAwsRequestId(): String {
        return "mockRequestID"
    }

    override fun getLogGroupName(): String {
        return "mockLogGroupName"
    }

    override fun getLogStreamName(): String {
        return "mockLogStreamName"
    }

    override fun getFunctionName(): String {
        return "mockGetFunctionName"
    }

    override fun getFunctionVersion(): String {
        return "mockFunctionVersion"
    }

    override fun getInvokedFunctionArn(): String {
        return "mockInvokedFunctionArn"
    }

    override fun getIdentity(): CognitoIdentity? {
        return null
    }

    override fun getClientContext(): ClientContext? {
        return null
    }

    override fun getRemainingTimeInMillis(): Int {
        return 0
    }

    override fun getMemoryLimitInMB(): Int {
        return 0
    }

    override fun getLogger(): LambdaLogger {
        return LambdaLoggerMock()
    }
}
