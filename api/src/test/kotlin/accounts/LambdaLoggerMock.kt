package accounts

import com.amazonaws.services.lambda.runtime.LambdaLogger

class LambdaLoggerMock : LambdaLogger {
    override fun log(message: String) {
        println(String.format("LambdaLoggerMock: %s", message))
    }

    override fun log(message: ByteArray) {
        this.log(String(message))
    }
}
