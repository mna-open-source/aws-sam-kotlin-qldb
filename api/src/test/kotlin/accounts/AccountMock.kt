package accounts

import accounts.dto.TransactionDTO
import accounts.dto.TransferDTO
import accounts.functions.anyTransactionDTO
import accounts.models.*
import java.util.*

data class AccountMock (
        override val accountId: String,
        override val balances: MutableList<Money> = ArrayList(),
        override val locks: MutableList<FundLock> = ArrayList(),
        val movements: MutableList<Movement> = ArrayList()
): IAccount {

    private val TYPE_DELIMITER = "."
    private val EMPTY = ""

    private fun addMovement(transaction: TransactionDTO): AccountMock = also {
        Movement(accountId = accountId, created = now(), transactionId = transaction.transactionId,
                money = transaction.money).also { movement ->
            movements.add(movement)
            upsertBalance(movement, movement.money.type)
        }
    }

    private fun upsertBalance(movement: Movement, type: String): AccountMock = also {
        balances.findLast { movement.money.copy(type).isCompatible(it) }.also { balance ->
            if (balance == null) {
                balances.add(movement.money.copy(type = type))
            } else {
                val newBalance = balance + movement.money
                balances.remove(balance)
                balances.add(newBalance)
            }
            type.takeIfHasParentType()?.also {
                upsertBalance(movement, it)
            }
        }
    }

    private fun String.takeIfHasParentType(): String? = substringBeforeLast(TYPE_DELIMITER, EMPTY)
            .takeIf { it.isNotEmpty() }

    fun mockCredit(transaction: TransactionDTO): AccountMock = addMovement(transaction)

    fun mockDebit(transaction: TransactionDTO): AccountMock = addMovement(-transaction)

    fun mockFundLock(money: Money) : AccountMock = also {
        locks.add(FundLock(created = now(), dueDate = now().plusSeconds(LOCK_DUE_SECONDS), money = money))
    }

    fun mockTransfer(transferDTO: TransferDTO, target: AccountMock): AccountMock = also {
        mockDebit(anyTransactionDTO(transactionId = transferDTO.transactionId, money = transferDTO.money))
        target.mockCredit(anyTransactionDTO(transactionId = transferDTO.transactionId, money = transferDTO.money))
    }

    override fun equals(other: Any?): Boolean {
        if (other == null)  return failedEquals("other is null")
        if (this === other) return true
        if (!IAccount::class.java.isAssignableFrom(other.javaClass)) return failedEquals("incompatible class")
        //if (other.javaClass != AccountMock::class.java
        //        && other.javaClass != Account::class.java ) return failedEquals("incompatible class")

        when(other) {
            is AccountMock -> {
                if (accountId != other.accountId) return propertyNotMatch("accountId")
                if (balances != other.balances) return propertyNotMatch("balances")
                if (locks != other.locks) return propertyNotMatch("locks")
                if (movements != other.movements) return propertyNotMatch("movements")
            }
            is Account -> {
                if (accountId != other.accountId) return propertyNotMatch("accountId")
                if (balances != other.balances) return propertyNotMatch("balances")
                if (locks != other.locks) return propertyNotMatch("locks")
            }
        }
        return true
    }

    private fun failedEquals(reason: String): Boolean = false.also {
        println(">> fail AccountMock.equals(). reason = $reason.")
    }

    private fun propertyNotMatch(property: String): Boolean = false.also {
        println(">> fail AccountMock.equals(). property '$property' not match.")
    }

    fun toAccount() = Account(accountId = accountId, balances = balances, locks = locks)
}

