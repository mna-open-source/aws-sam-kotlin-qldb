#!/bin/bash


functionName="Accounts"

caseName=$1
if [[ -z $caseName ]]
 then
  caseName="default"
fi

fullFunctionName="${functionName}Function"
eventFile="api/src/test/resources/events/${functionName}/${caseName}.json"
#IP_ADDRESS=$(ip addr show | grep "\binet\b.*\bdocker0\b" | awk '{print $2}' | cut -d '/' -f 1)
#parametersOverride="EnvironmentStackName=dev"

echo "Invoke ${fullFunctionName}"
echo "event: ${eventFile}"
echo "parameters: ${parametersOverride}"

#sam local invoke $fullFunctionName -e $eventFile --parameter-overrides $parametersOverride
sam local invoke $fullFunctionName -e $eventFile

if [ $? -eq 0 ]; then
    echo "Invoke SUCCESS!"
else
    echo "Invoke FAIL"
    echo
    echo "Available functions to test:"
    cat template.yaml | grep "Function::" | sed 's/Handler: accounts//' | sed 's/Function::handleRequest//' | sed 's/\./@/g' | sed 's/ @.*@//'
fi
