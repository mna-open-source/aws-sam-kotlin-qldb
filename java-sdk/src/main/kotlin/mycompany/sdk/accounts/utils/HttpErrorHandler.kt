package mycompany.sdk.accounts.utils

import com.fasterxml.jackson.databind.JsonNode
import org.springframework.http.HttpStatus.*
import org.springframework.web.reactive.function.client.ClientResponse
import reactor.core.publisher.Mono

object HttpErrorHandler {

    fun defaultHandling(clientResponse: ClientResponse): Mono<out Throwable> {
        return when (clientResponse.statusCode()) {
            UNAUTHORIZED -> Mono.error(AccessDenied)
            FORBIDDEN -> Mono.error(Forbidden(clientResponse.statusCode().reasonPhrase))
            BAD_REQUEST, CONFLICT -> defaultBadRequestAndConflictHandling(clientResponse)
            else -> Mono.error(UnexpectedResponse(clientResponse.statusCode().toString()))
        }
    }

    private fun defaultBadRequestAndConflictHandling(r: ClientResponse): Mono<out Throwable> {
        return r
            .bodyToMono(JsonNode::class.java)
            .map { body: JsonNode ->
                when {
                    body.has("errors") -> parseError(body["errors"].iterator().next())
                    body.isArray -> parseError(body.iterator().next())
                    else -> parseUnknownError(body)
                }
            }
            .defaultIfEmpty(unknownError("response without body"))
            .handle { e, sink -> sink.error(e) }
    }

    private fun parseError(error: JsonNode): Throwable {
        return GenericSdkError(
            error["entity"].asText(),
            error["property"].asText(),
            error["invalidValue"].asText(null),
            error["message"].asText()
        )
    }

    private fun parseUnknownError(body: JsonNode): Throwable {
        return unknownError(body["message"]?.asText("unknown")
            ?: "error without message")
    }

    private fun unknownError(errorMessage: String): Throwable {
        return GenericSdkError(
            "unknown",
            "",
            "",
            errorMessage
        )
    }
}

object AccessDenied : RuntimeException()
class Forbidden(message: String) : RuntimeException(message)
class UnexpectedResponse(message: String) : RuntimeException(message)
