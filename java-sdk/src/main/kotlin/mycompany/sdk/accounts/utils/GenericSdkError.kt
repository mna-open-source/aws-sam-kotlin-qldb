package mycompany.sdk.accounts.utils

data class GenericSdkError(
    val entity: String,
    val property: String,
    val invalidValue: String?,
    val errorMessage: String
) : RuntimeException()
