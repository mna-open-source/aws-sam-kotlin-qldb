package mycompany.sdk.accounts.utils

import org.springframework.web.reactive.function.client.*
import org.springframework.web.util.DefaultUriBuilderFactory
import org.springframework.web.util.DefaultUriBuilderFactory.EncodingMode.VALUES_ONLY
import reactor.core.publisher.Mono
import java.net.URI
import java.util.*

object WebClientBuilderCustomizer {

    fun customize(builder: WebClient.Builder, root: URI): WebClient.Builder {
        return builder
            .clone()
            .uriBuilderFactory(DefaultUriBuilderFactory(root.toString()).also { VALUES_ONLY })
            .baseUrl(Objects.requireNonNull(root.toString()))
            .filter(ForbiddenHostsFilter(root.host))
    }

    class ForbiddenHostsFilter(private val allowedHost: String) : ExchangeFilterFunction {
        override fun filter(request: ClientRequest, next: ExchangeFunction): Mono<ClientResponse> {
            val uri = request.url()
            if (uri.host == null || !uri.host.equals(allowedHost, ignoreCase = true)) throw Forbidden("Forbidden host: $uri")
            return next.exchange(request)
        }
    }
}
