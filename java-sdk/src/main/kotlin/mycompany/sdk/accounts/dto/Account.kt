package mycompany.sdk.accounts.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.ArrayList

interface IAccount {
    val accountId: String
    val balances: List<Money>
    val locks: List<FundLock>
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class Account @JsonCreator constructor(

    @field:JsonProperty("account_id")
    @param:JsonProperty("account_id")
    override val accountId: String,

    @field:JsonProperty("balances")
    @param:JsonProperty("balances")
    override val balances: List<Money> = ArrayList(),

    @field:JsonProperty("available")
    @param:JsonProperty("available")
    val available: List<Money> = ArrayList(),

    @field:JsonProperty("locks")
    @param:JsonProperty("locks")
    override val locks: List<FundLock> = ArrayList()
) : IAccount
