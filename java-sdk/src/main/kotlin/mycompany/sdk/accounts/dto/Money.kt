package mycompany.sdk.accounts.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Money @JsonCreator constructor(

    @field:JsonProperty("type")
    @param:JsonProperty("type")
    val type: String,

    @field:JsonProperty("currency")
    @param:JsonProperty("currency")
    val currency: String,

    @field:JsonProperty("amount")
    @param:JsonProperty("amount")
    val amount: Double
) {
    fun isCompatible(other: Money) = ((type == other.type) && (currency == other.currency))

    @JsonIgnore
    val key = "${type}:${currency}"

    operator fun unaryMinus() = copy(amount = amount * -1.00)

    operator fun compareTo(other: Money) = amount.compareTo(other.amount)

    operator fun minus(other: Money) = copy(amount = amount - other.amount)

    operator fun plus(other: Money) = copy(amount = amount + other.amount)

    operator fun minus(other: Double) = copy(amount = amount - other)

    operator fun plus(other: Double) = copy(amount = amount + other)

    operator fun times(other: Double) = copy(amount = amount * other)
}
