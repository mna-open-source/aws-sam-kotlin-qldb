package mycompany.sdk.accounts.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class FundLock @JsonCreator constructor(

    @field:JsonProperty("id")
    @param:JsonProperty("id")
    val id: UUID = UUID.randomUUID(),

    @field:JsonProperty("created")
    @param:JsonProperty("created")
    val created: LocalDateTime,

    @field:JsonProperty("dueDate")
    @param:JsonProperty("dueDate")
    val dueDate: LocalDateTime,

    @field:JsonProperty("money")
    @param:JsonProperty("money")
    val money: Money
)
