package mycompany.sdk.accounts

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import mycompany.sdk.accounts.dto.Account
import mycompany.sdk.accounts.dto.Money
import mycompany.sdk.accounts.utils.HttpErrorHandler
import mycompany.sdk.accounts.utils.WebClientBuilderCustomizer

import java.net.URI
import java.util.*

interface AccountSdk {

    fun findById(accountId: String, accessToken: String): Mono<Account>
    fun applyCredit(accountId: String, transactionId: String, money: Money, accessToken: String): Mono<Unit>
    fun applyDebit(accountId: String, transactionId: String, money: Money, accessToken: String): Mono<Unit>
    fun applyLock(accountId: String, money: Money, accessToken: String): Mono<UUID>
    fun releaseLock(accountId: String, lockId: UUID, accessToken: String): Mono<Unit>
    fun transfer(
        sourceAccountId: String, targetAccountId: String, transactionId: String,
        money: Money, accessToken: String
    ): Mono<Unit>

    fun transfer(sourceAccountId: String, transfers: List<TransferDTO>, accessToken: String): Mono<Unit>
}

class HttpAccountSdk(
    root: URI,
    builder: WebClient.Builder
) : AccountSdk {
    companion object {

        const val BASE_PATH = "/accounts"
        const val BASE_RESOURCE = "$BASE_PATH/{accountId}"
    }

    private val http: WebClient = WebClientBuilderCustomizer.customize(builder, root).build()

    override fun findById(accountId: String, accessToken: String): Mono<Account> = http.get()
        .uri { it.path(BASE_RESOURCE).build(accountId) }
        .header(AUTHORIZATION, "Bearer $accessToken")
        .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
        .retrieve()
        .onStatus(this::NOT_OK, HttpErrorHandler::defaultHandling)
        .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
        .bodyToMono(Account::class.java)


    override fun applyCredit(accountId: String, transactionId: String, money: Money, accessToken: String): Mono<Unit> =
        TransactionDTO(transactionId = transactionId, money = money).let { transaction ->
            http.post()
                .uri { it.path("$BASE_RESOURCE/credits").build(accountId) }
                .header(AUTHORIZATION, "Bearer $accessToken")
                .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                .body(BodyInserters.fromObject(transaction))
                .retrieve()
                .onStatus(this::NOT_NO_CONTENT, HttpErrorHandler::defaultHandling)
                .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
                .bodyToMono(JsonNode::class.java)
                .flatMap { Mono.empty() }
        }

    override fun applyDebit(accountId: String, transactionId: String, money: Money, accessToken: String): Mono<Unit> =
        TransactionDTO(transactionId = transactionId, money = money).let { transaction ->
            http.post()
                .uri { it.path("$BASE_RESOURCE/debits").build(accountId) }
                .header(AUTHORIZATION, "Bearer $accessToken")
                .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                .body(BodyInserters.fromObject(transaction))
                .retrieve()
                .onStatus(this::NOT_NO_CONTENT, HttpErrorHandler::defaultHandling)
                .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
                .bodyToMono(JsonNode::class.java)
                .flatMap { Mono.empty() }
        }

    override fun applyLock(accountId: String, money: Money, accessToken: String): Mono<UUID> = http.post()
        .uri { it.path("$BASE_RESOURCE/locks").build(accountId) }
        .header(AUTHORIZATION, "Bearer $accessToken")
        .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
        .body(BodyInserters.fromObject(money))
        .retrieve()
        .onStatus(this::NOT_OK, HttpErrorHandler::defaultHandling)
        .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
        .bodyToMono(UUID::class.java)

    override fun releaseLock(accountId: String, lockId: UUID, accessToken: String): Mono<Unit> = http.delete()
        .uri { it.path("$BASE_RESOURCE/locks/{lockId}").build(accountId, lockId.toString()) }
        .header(AUTHORIZATION, "Bearer $accessToken")
        .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
        .retrieve()
        .onStatus(this::NOT_NO_CONTENT, HttpErrorHandler::defaultHandling)
        .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
        .bodyToMono(JsonNode::class.java)
        .flatMap { Mono.empty() }

    override fun transfer(
        sourceAccountId: String, targetAccountId: String, transactionId: String,
        money: Money, accessToken: String
    ): Mono<Unit> =
        TransferDTO(
            target = targetAccountId, transactionId = transactionId,
            money = money
        ).let { transferDTO ->
            transfer(sourceAccountId, listOf(transferDTO), accessToken)
        }

    override fun transfer(
        sourceAccountId: String, transfers: List<TransferDTO>,
        accessToken: String
    ): Mono<Unit> =
        http.post()
            .uri { it.path("$BASE_RESOURCE/transfers").build(sourceAccountId) }
            .header(AUTHORIZATION, "Bearer $accessToken")
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .body(BodyInserters.fromObject(transfers))
            .retrieve()
            .onStatus(this::NOT_NO_CONTENT, HttpErrorHandler::defaultHandling)
            .onStatus(HttpStatus::isError, HttpErrorHandler::defaultHandling)
            .bodyToMono(JsonNode::class.java)
            .flatMap { Mono.empty() }


    fun NOT_NO_CONTENT(status: HttpStatus): Boolean = HttpStatus.NO_CONTENT != status

    fun NOT_OK(status: HttpStatus): Boolean = HttpStatus.OK != status

    private data class TransactionDTO(

        @field:JsonProperty("transaction_id")
        @param:JsonProperty("transaction_id")
        val transactionId: String,

        @field:JsonProperty("money")
        @param:JsonProperty("money")
        val money: Money
    )
}

data class TransferDTO(

    @field:JsonProperty("transaction_id")
    @param:JsonProperty("transaction_id")
    val transactionId: String,

    @field:JsonProperty("target")
    @param:JsonProperty("target")
    val target: String,

    @field:JsonProperty("money")
    @param:JsonProperty("money")
    val money: Money
)
