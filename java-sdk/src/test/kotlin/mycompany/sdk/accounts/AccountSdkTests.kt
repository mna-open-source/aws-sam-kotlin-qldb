package mycompany.sdk.accounts

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.matching.EqualToPattern
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import mycompany.sdk.accounts.HttpAccountSdk.Companion.BASE_PATH
import mycompany.sdk.accounts.HttpAccountSdk.Companion.BASE_RESOURCE
import java.net.URI
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class AccountSdkTests {

    @Rule
    @JvmField
    var wireMockRule: WireMockRule = WireMockRule()

    private lateinit var sut: HttpAccountSdk

    @Before
    fun setup() {
        sut = HttpAccountSdk(URI.create(wireMockRule.baseUrl()), WebClient.builder())
    }

    @Test
    fun `Returns 200 with empty account`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            stubFor(
                get(urlPathMatching("$BASE_PATH/[^/]+"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(200)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .withBody(expectedAccount.toJsonString())
                    )
            )


            StepVerifier
                .create(sut.findById(expectedAccount.accountId, token))
                .assertNext { actualAccount ->
                    assertEquals(expectedAccount, actualAccount)
                }
                .verifyComplete()
        }
    }

    @Test
    fun `returns 200 with account with funds`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            val amount1 = anyMoney()
            val amount2 = anyMoney()

            expectedAccount
                .mockCredit(transactionId = anyString(), money = amount1)
                .mockCredit(transactionId = anyString(), money = amount2)
                .mockFundLock(amount1 * 0.25)
                .mockFundLock(amount2 * 0.5)
                .mockFundLock(amount2 * 0.5)

            stubFor(
                get("$BASE_PATH/${expectedAccount.accountId}")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(200)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .withBody(expectedAccount.toJsonString())
                    )
            )

            StepVerifier
                .create(sut.findById(expectedAccount.accountId, token))
                .assertNext { actualAccount ->
                    println("expected: ${expectedAccount.toJsonString()} -class: ${expectedAccount.javaClass.simpleName}")
                    println("actual:   ${actualAccount.toJsonString()} -class: ${actualAccount.javaClass.simpleName}")
                    assertEquals(expectedAccount, actualAccount)
                }
                .verifyComplete()
        }
    }

    @Test
    fun `returns 204 when apply credit`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            stubFor(
                post("$BASE_PATH/${expectedAccount.accountId}/credits")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(204)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
            )


            StepVerifier
                .create(
                    sut.applyCredit(
                        accountId = expectedAccount.accountId, transactionId = anyUUID().toString(),
                        money = anyMoney(), token
                    )
                )
                .verifyComplete()

        }
    }

    @Test
    fun `returns 204 when apply debit`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            stubFor(
                post("$BASE_PATH/${expectedAccount.accountId}/debits")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(204)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
            )

            StepVerifier
                .create(
                    sut.applyDebit(
                        accountId = expectedAccount.accountId, transactionId = anyUUID().toString(),
                        money = anyMoney(), token
                    )
                )
                .verifyComplete()

        }
    }

    @Test
    fun `returns 200 when apply lock`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            val expectedLockId = anyUUID()

            stubFor(
                post("$BASE_PATH/${expectedAccount.accountId}/locks")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(200)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .withBody(expectedLockId.toJsonString())
                    )
            )

            StepVerifier
                .create(sut.applyLock(accountId = expectedAccount.accountId, money = anyMoney(), token))
                .assertNext { actualLockId ->
                    assertEquals(expectedLockId, actualLockId)
                }
                .verifyComplete()

        }
    }

    @Test
    fun `returns 204 when release lock`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            val lockId = anyUUID()

            stubFor(
                delete("$BASE_PATH/${expectedAccount.accountId}/locks/${lockId.toString()}")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(204)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
            )


            StepVerifier
                .create(sut.releaseLock(accountId = expectedAccount.accountId, lockId = lockId, token))
                .verifyComplete()

        }
    }

    @Test
    fun `returns 204 when transfer`() {

        withPair(anyString(), anyAccount()) { token, expectedAccount ->

            stubFor(
                post("$BASE_PATH/${expectedAccount.accountId}/transfers")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        aResponse()
                            .withStatus(204)
                            .withHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
            )

            StepVerifier
                .create(
                    sut.transfer(
                        sourceAccountId = expectedAccount.accountId,
                        targetAccountId = anyAccount().accountId,
                        transactionId = anyUUID().toString(),
                        money = anyMoney(), token
                    )
                )
                .verifyComplete()

        }
    }

}
