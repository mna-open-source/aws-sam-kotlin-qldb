package mycompany.sdk.accounts

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import mycompany.sdk.accounts.dto.Money
import java.time.Clock
import java.time.LocalDateTime
import java.util.*
import kotlin.math.roundToInt
import kotlin.random.Random

object TestExtensions {
    val logger: Logger = LoggerFactory.getLogger(TestExtensions::class.java)
}

fun now(): LocalDateTime = LocalDateTime.now(Clock.systemUTC())

fun anyUUID(): UUID = UUID.randomUUID()

fun anyString(): String = anyUUID().toString().replace("-","")

fun anyValidJWT(): String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ3YWJpcGF5LWJhY2tlbmQifQ.xC46aJsQhLN1pfJaDwcV-EBpKkrPxUnkobN-cAzMQyk"

fun anyTemporalJsonFileName() = "/tmp/${anyString()}.json"

fun anyAmount() : Double = (Random.nextDouble(0.01, 1000.00) * 100).roundToInt() / 100.0

fun anyAccountId(): String = "tmp_${anyUUID().toString()}"

fun anyAccount(): AccountMock = AccountMock(accountId = anyAccountId())

fun anyMoney(type: String = anyString(), currency: String = anyString(), amount: Double = anyAmount()): Money =
        Money(type = type, currency = currency, amount = amount)

fun anyTransferDTO(transactionId: String = anyString(), targetAccount: String = anyAccountId(),
                   money: Money = anyMoney()
): TransferDTO =
        TransferDTO(transactionId = transactionId, target = targetAccount, money = money)

fun <F, S, R> withPair(first: F, second: S, block: (F, S) -> R): R  = block(first, second)

fun anyGetApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("defaultGet"))

fun anyPostApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("defaultPost"))

fun anyReleaseLockApiGatewayEvent(): APIGatewayProxyRequestEvent = deserializeEvent(loadEventResource("releaseLock"))

private fun loadEventResource(eventName: String) = TestExtensions::class.java.getResource("/events/${eventName}.json")?.readText()

private fun deserializeEvent(eventAsJson: String?) = Mapper.readValue(eventAsJson, APIGatewayProxyRequestEvent::class.java)

fun APIGatewayProxyRequestEvent.setAccountId(accountId: String) = also { event ->
    event.path = event.path.replace("987987987", accountId)
    event.pathParameters["accountId"] = accountId
}

fun APIGatewayProxyRequestEvent.setLockId(lockId: UUID) = also { event ->
    lockId.toString().also {
        event.path = event.path.replace("b893ab93-a418-4412-8cad-e677a7432eea", it)
        event.pathParameters["lockId"] = it
    }
}

fun APIGatewayProxyRequestEvent.setBody(element: Any) = also { event ->
    event.body = Mapper.writeValueAsString(element)
}
