package mycompany.sdk.accounts

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.github.tomakehurst.wiremock.matching.EqualToPattern
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.test.StepVerifier
import mycompany.sdk.accounts.HttpAccountSdk.Companion.BASE_RESOURCE
import mycompany.sdk.accounts.utils.GenericSdkError
import mycompany.sdk.accounts.utils.UnexpectedResponse
import wiremockLambda.SamInvokerExtension
import wiremockLambda.lambdaFetchedResponseForPath
import java.net.URI
import kotlin.test.assertEquals

@Ignore
@RunWith(MockitoJUnitRunner.StrictStubs::class)
class AccountSdkIntegrationTests {

    @Rule
    @JvmField
    var wireMockRule: WireMockRule = WireMockRule(options().extensions(SamInvokerExtension()))

    private lateinit var sut: HttpAccountSdk

    @Before
    fun setup() {
        sut = HttpAccountSdk(URI.create(wireMockRule.baseUrl()), WebClient.builder())
    }

    @Test
    fun `SAM Returns success with empty account`() {

        withPair(anyValidJWT(), anyAccount()) { token, expectedAccount ->

            stubFor(
                get(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))
                    .willReturn(
                        lambdaFetchedResponseForPath(BASE_RESOURCE)
                    )
            )

            StepVerifier
                .create(sut.findById(expectedAccount.accountId, token))
                .assertNext { actualAccount ->
                    assertEquals(expectedAccount, actualAccount)
                }
                .verifyComplete()
        }
    }

    @Test
    fun `SAM returns success with account with funds`() {

        withPair(anyValidJWT(), anyAccount()) { token, expectedAccount ->

            val amount1 = anyMoney()
            val amount2 = amount1 * 0.5

            expectedAccount
                .mockCredit(transactionId = anyString(), money = amount1)
                .mockFundLock(amount2)

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/credits"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/credits")
                    )
            )

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/locks"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/locks")
                    )
            )

            stubFor(
                get("${HttpAccountSdk.BASE_PATH}/${expectedAccount.accountId}")
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath(BASE_RESOURCE)
                    )
            )

            StepVerifier
                .create(sut.applyCredit(expectedAccount.accountId, anyString(), amount1, token))
                .verifyComplete()

            StepVerifier
                .create(sut.applyLock(expectedAccount.accountId, amount2, token))
                .assertNext { lockId -> println("lock id: $lockId") }
                .verifyComplete()


            StepVerifier
                .create(sut.findById(expectedAccount.accountId, token))
                .assertNext { actualAccount ->
                    println("expected: ${expectedAccount.toJsonString()} -class: ${expectedAccount.javaClass.simpleName}")
                    println("actual:   ${actualAccount.toJsonString()} -class: ${actualAccount.javaClass.simpleName}")
                    assertEquals(expectedAccount, actualAccount)
                }
                .verifyComplete()
        }
    }

    @Test
    fun `SAM returns success with apply credit`() {

        withPair(anyValidJWT(), anyAccountId()) { token, accountId ->

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/credits"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/credits")
                    )
            )

            StepVerifier
                .create(sut.applyCredit(accountId, anyString(), anyMoney(), token))

                .verifyComplete()
        }
    }

    @Test
    fun `SAM returns error with apply debit`() {

        withPair(anyValidJWT(), anyAccountId()) { token, accountId ->

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/debits"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/debits")
                    )
            )

            StepVerifier
                .create(sut.applyDebit(accountId, anyString(), anyMoney(), token))

                .verifyErrorMatches { t ->
                    (t.javaClass.isAssignableFrom(GenericSdkError::class.java)
                            && with(t as GenericSdkError) {
                        errorMessage.contains("Insufficient Funds")
                    }
                            )
                }
        }
    }

    @Test
    fun `SAM returns error with apply lock`() {

        withPair(anyValidJWT(), anyAccountId()) { token, accountId ->

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/locks"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/locks")
                    )
            )

            StepVerifier
                .create(sut.applyLock(accountId, anyMoney(), token))

                .verifyErrorMatches { t ->
                    (t.javaClass.isAssignableFrom(GenericSdkError::class.java)
                            && with(t as GenericSdkError) {
                        errorMessage.contains("Insufficient Funds")
                    }
                            )
                }
        }
    }

    @Test
    fun `SAM returns error with release lock`() {

        withPair(anyValidJWT(), anyAccountId()) { token, accountId ->

            stubFor(
                delete(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/locks/[^/]+"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/locks/{lockId}")
                    )
            )

            StepVerifier
                .create(sut.releaseLock(accountId, anyUUID(), token))

                .verifyErrorMatches { t ->
                    (t.javaClass.isAssignableFrom(UnexpectedResponse::class.java)
                            && with(t as UnexpectedResponse) {
                        this.message!!.contains("404")

                    }
                            )
                }
        }
    }

    @Test
    fun `SAM returns error with simple transfer`() {

        withPair(anyValidJWT(), anyAccountId()) { token, accountId ->

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/transfers"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/transfers")
                    )
            )

            StepVerifier
                .create(sut.transfer(accountId, anyAccountId(), anyString(), anyMoney(), token))

                .verifyErrorMatches { t ->
                    (t.javaClass.isAssignableFrom(GenericSdkError::class.java)
                            && with(t as GenericSdkError) {
                        errorMessage.contains("Insufficient Funds")
                    }
                            )
                }
        }
    }

    @Test
    fun `SAM returns error with multi transfer`() {

        withPair(anyValidJWT(), anyAccountId()) { token, accountId ->

            stubFor(
                post(urlPathMatching("${HttpAccountSdk.BASE_PATH}/[^/]+/transfers"))
                    .withHeader(AUTHORIZATION, equalTo("Bearer $token"))
                    .withHeader(CONTENT_TYPE, EqualToPattern(MediaType.APPLICATION_JSON_VALUE))

                    .willReturn(
                        lambdaFetchedResponseForPath("$BASE_RESOURCE/transfers")
                    )
            )

            val firstTransferToTest = anyTransferDTO()
            val secondTransferToTest = anyTransferDTO()

            StepVerifier
                .create(sut.transfer(accountId, listOf(firstTransferToTest, secondTransferToTest), token))

                .verifyErrorMatches { t ->
                    (t.javaClass.isAssignableFrom(GenericSdkError::class.java)
                            && with(t as GenericSdkError) {
                        errorMessage.contains("Insufficient Funds")
                    }
                            )
                }
        }
    }
}
