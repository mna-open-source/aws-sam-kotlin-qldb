package mycompany.sdk.accounts


import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import mycompany.sdk.accounts.dto.Account
import mycompany.sdk.accounts.dto.FundLock
import mycompany.sdk.accounts.dto.IAccount
import mycompany.sdk.accounts.dto.Money
import java.time.LocalDateTime
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class AccountMock @JsonCreator constructor(

    @field:JsonProperty("account_id")
    @param:JsonProperty("account_id")
    override val accountId: String,

    @field:JsonProperty("balances")
    @param:JsonProperty("balances")
    override val balances: MutableList<Money> = ArrayList(),

    @field:JsonProperty("locks")
    @param:JsonProperty("locks")
    override val locks: MutableList<FundLock> = ArrayList(),

    val movements: MutableList<Movement> = ArrayList()
) : IAccount {
    companion object {
        const val LOCK_DUE_SECONDS: Long = 3600
    }

    val available: List<Money>
        @JsonProperty("available")
        get() = availableBalances()

    fun sumByLocks(): List<Money> = now().let { date ->
        locks.filter { it.dueDate > date }
            .map { it.money }
    }
        .groupingBy { it.key }
        .reduce { _, accumulator, element -> accumulator + element }
        .values.toList()
        .map { -it }

    fun availableBalances(): List<Money> = (balances + sumByLocks())
        .groupingBy { it.key }
        .reduce { _, accumulator, element -> accumulator + element }
        .values.toList()

    private fun addMovement(transactionId: String, money: Money): AccountMock = also {

        movements.add(
            Movement(
                accountId = accountId, created = now(), transactionId = transactionId,
                money = money
            )
        )

        balances.findLast { money.isCompatible(it) }.also { balance ->
            if (balance == null) {
                balances.add(money)
            } else {
                val newBalance = balance + money
                balances.remove(balance)
                balances.add(newBalance)
            }
        }
    }

    fun mockCredit(transactionId: String, money: Money): AccountMock = addMovement(transactionId, money)

    fun mockDebit(transactionId: String, money: Money): AccountMock = addMovement(transactionId, -money)

    fun mockFundLock(money: Money): AccountMock = also {
        locks.add(FundLock(created = now(), dueDate = now().plusSeconds(LOCK_DUE_SECONDS), money = money))
    }

    fun mockTransfer(target: AccountMock, transactionId: String, money: Money): AccountMock = also {
        mockDebit(transactionId = transactionId, money = money)
        target.mockCredit(transactionId = transactionId, money = money)
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return failedEquals("other is null")
        if (this === other) return true
        if (!IAccount::class.java.isAssignableFrom(other.javaClass)) return failedEquals("incompatible class")

        when (other) {
            is AccountMock -> {
                if (accountId != other.accountId) return propertyNotMatch("accountId")
                if (balances != other.balances) return propertyNotMatch("balances")
                if (locksNotMatch(other.locks)) return propertyNotMatch("lock")
                if (available != other.available) return propertyNotMatch("available")
                if (movements != other.movements) return propertyNotMatch("movements")
            }
            is Account -> {
                if (accountId != other.accountId) return propertyNotMatch("accountId")
                if (balances != other.balances) return propertyNotMatch("balances")
                if (locksNotMatch(other.locks)) return propertyNotMatch("lock")
                if (available != other.available) return propertyNotMatch("available")
            }
        }
        return true
    }

    private fun locksNotMatch(otherLocks: List<FundLock>): Boolean {
        if (locks.size != otherLocks.size) return false
        var result = true
        locks.forEach { lock ->
            result = result && (otherLocks.firstOrNull { it.money == lock.money } != null)
        }
        return !result
    }

    private fun failedEquals(reason: String): Boolean = false.also {
        println(">> fail AccountMock.equals(). reason = $reason.")
    }

    private fun propertyNotMatch(property: String): Boolean = false.also {
        println(">> fail AccountMock.equals(). property '$property' not match.")
    }
}

data class Movement(
    val accountId: String,
    val created: LocalDateTime,
    val money: Money,
    val transactionId: String
)

