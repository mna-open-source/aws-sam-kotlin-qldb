package wiremockLambda

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.common.FileSource
import com.github.tomakehurst.wiremock.extension.Parameters
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer
import com.github.tomakehurst.wiremock.http.HttpHeader
import com.github.tomakehurst.wiremock.http.HttpHeaders
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.ResponseDefinition
import mycompany.sdk.accounts.Mapper
import mycompany.sdk.accounts.anyTemporalJsonFileName
import mycompany.sdk.accounts.deserializeAs
import mycompany.sdk.accounts.toJsonString
import org.springframework.http.MediaType
import org.springframework.web.util.UriTemplate
import java.io.BufferedReader
import java.io.File
import java.io.InputStream
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors

const val REQUEST_PATH_PARAMETER = "REQUEST_PATH"

class SamInvokerExtension : ResponseTemplateTransformer(true) {

    override fun transform(
        request: Request, responseDefinition: ResponseDefinition,
        files: FileSource?, parameters: Parameters
    ): ResponseDefinition =
        execute(
            buildCommandToRun(
                renderApiGatewayProxyRequestEvent(request, parameters)
                    .saveToDisk()
            )
        )?.deserializeAs(APIGatewayProxyResponseEvent::class.java)
            ?.let { eventResponse ->
                ResponseDefinitionBuilder()
                    .withStatus(eventResponse.statusCode)
                    .withBody(eventResponse.body)
                    .withHeaders(eventResponse.headers.toHeaders())
                    .build()
            } ?: super.transform(request, responseDefinition, files, parameters)


    private fun getPathParameters(uri: String, template: String): Map<String, String>? =
        UriTemplate(template).match(uri)

    private fun renderApiGatewayProxyRequestEvent(
        request: Request,
        parameters: Parameters
    ): APIGatewayProxyRequestEvent =
        getApiGatewayEventTemplate().also {
            with(it) {
                resource = parameters[REQUEST_PATH_PARAMETER]!! as String
                path = request.url
                httpMethod = request.method.value()
                headers =
                    request.headers.all().stream().collect(Collectors.toMap(HttpHeader::key, HttpHeader::firstValue))
                pathParameters = getPathParameters(uri = path, template = resource)
                requestContext.resourcePath = resource
                requestContext.httpMethod = httpMethod
                requestContext.path = resource
                body = request.bodyAsString
            }
        }

    private fun APIGatewayProxyRequestEvent.saveToDisk(): String = anyTemporalJsonFileName().also {
        File(it).writeText(this.toJsonString())
    }

    private fun buildCommandToRun(filename: String) = listOf(
        "sam", "local", "invoke",
        "AccountsFunction", "-e", filename
    )

    private fun execute(command: List<String>): String? {

        var invokeResult: String? = null

        val builder = ProcessBuilder(command)
        builder.directory(File("../"))
        builder.redirectErrorStream(true)
        try {
            val process = builder.start()
            val stdout: InputStream = process.inputStream
            val reader = BufferedReader(InputStreamReader(stdout))
            process.waitFor(30, TimeUnit.SECONDS)
            var line: String? = reader.readLine()

            while (line != null) {
                println("Stdout: $line");
                if (line.contains("{\"statusCode")) {
                    invokeResult = line
                }
                line = reader.readLine()
            }

            if (process.exitValue() != 0) {
                throw RuntimeException("Unable to invoke SAM CLI  - exit code: ${process.exitValue()}")
            }
            return invokeResult

        } catch (e: Throwable) {
            e.printStackTrace()
            throw RuntimeException("Unable to invoke SAM CLI: ${e.message}")
        }
    }
}

class SamInvokerResponseDefinitionBuilder(
    val requestPath: String
) : ResponseDefinitionBuilder() {

    override fun build(): ResponseDefinition {
        val httpHeaders =
            if (headers == null || headers.isEmpty()) null else com.github.tomakehurst.wiremock.http.HttpHeaders(headers)
        val transformerParameters = Parameters.one(REQUEST_PATH_PARAMETER, requestPath)
        return ResponseDefinition(
            status, statusMessage, stringBody, null, base64Body,
            bodyFileName, httpHeaders, null, fixedDelayMilliseconds,
            delayDistribution, chunkedDribbleDelay, proxyBaseUrl, fault, responseTransformerNames,
            transformerParameters, wasConfigured
        )
    }
}

fun lambdaFetchedResponseForPath(path: String) = SamInvokerResponseDefinitionBuilder(requestPath = path)

private fun MutableMap<String, String>.toHeaders(): HttpHeaders = also {
    it.put(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
}.map { entry ->
    HttpHeader(entry.key, entry.value)
}.let { HttpHeaders(it) }

private fun getApiGatewayEventTemplate(): APIGatewayProxyRequestEvent =
    deserializeEvent(loadEventResource("ApiGatewayProxyRequestEventTemplate"))

private fun loadEventResource(eventName: String) =
    SamInvokerExtension::class.java.getResource("/templates/${eventName}.json")?.readText()

private fun deserializeEvent(eventAsJson: String?) =
    Mapper.readValue(eventAsJson, APIGatewayProxyRequestEvent::class.java)

