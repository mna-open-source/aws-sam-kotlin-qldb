plugins {
    kotlin("jvm")
    `java-library`
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.5.21")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:1.5.21")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.30.1")
    testImplementation("io.projectreactor:reactor-test:3.4.9")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.slf4j:slf4j-simple:1.7.32")
    testImplementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.12.5")
    testImplementation("com.amazonaws:aws-lambda-java-events:3.10.0")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
}

val compileKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
    apiVersion = "1.3"
}

val compileTestKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
    apiVersion = "1.3"
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.test {
    reports {
        junitXml.isEnabled = true
    }
    testLogging {
        events = setOf(org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED)
        showExceptions = true
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
}

sdkPublisher {
    artifact.id = "accounts-sdk"
    artifact.groupId = "my-company"
}
